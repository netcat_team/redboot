/*
 * fat.h
 *
 *  Created on: Apr 8, 2017
 *      Author: netcat
 */

#ifndef FAT_H
#define FAT_H

#include "fatfs_ms.h"
#include "hw_defs.h"

#define FAT_CLUSTER_SIZE 512
#define FAT_ROOT_CLUSTER 2
#define FAT_EOC1 0x0FFFFFF8
#define FAT_EOC2 0x0FFFFFFF
#define FAT_EOCF -1

typedef struct
{
  uint32_t pos; // current seek position
  uint32_t addr; // start of file cluster
  uint32_t size; // total file size
  uint32_t buf_addr;
  uint8_t buf[FAT_CLUSTER_SIZE];
} fatfs_file_t;

typedef struct
{
  // buf
  uint8_t buf[FAT_CLUSTER_SIZE];

  // addr
  uint32_t fs_addr;
  uint32_t data_addr;

  // LFN buf
  uint8_t lnf_buf[256];
  uint32_t lnf_len;

  // params
  uint32_t sector_in_cluster;

  // dev
  io_blk_t *io;
} fatfs_t;

fatfs_file_t *fat_open(fatfs_t *fs, fatfs_file_t *file, const char *path);
uint32_t fat_seek(fatfs_t *fs, fatfs_file_t *file, uint32_t len);
uint32_t fat_read(fatfs_t *fs, fatfs_file_t *file, uint8_t *buf, uint32_t size);

uint32_t fat_init(fatfs_t *fs, uint32_t addr);
void fat_cluster(fatfs_t *fs, void *buf, uint32_t cluster);
uint32_t fat_table(fatfs_t *fs, void *buf, uint32_t cluster);
fat_dir_t *fat_find_path(fatfs_t *fs, const char *path);

#endif
