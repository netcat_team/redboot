/*
 * vfs.c
 *
 *  Created on: Apr 27, 2020
 *      Author: netcat
 */

#include "syslog.h"
#include "hw_sdio.h"
#include "mbr.h"
#include "vfs.h"
#include "fatfs.h"

io_blk_t sdio = { sdio_read, 0 };
fatfs_t fatfs = { .io = &sdio };

void mbr_cb(uint32_t addr, uint8_t type)
{
  switch(type)
  {
    case MBR_PART_TYPE_FAT32:
      fat_init(&fatfs, addr);
      break;

    default:
      break;
  }
}

void vfs_init()
{
  if(!mbr_find_part(&sdio, mbr_cb))
  {
    syslog(SYSLOG_ERR, "[vfs]: no any fs found in /sdio\n");
  }
}

FILE *fopen(const char *filename, const char *mode)
{
  (void)mode;
  fatfs_file_t *f = malloc(sizeof(fatfs_file_t));
  if(!fat_open(&fatfs, f, filename))
  {
    free(f);
    return 0;
  }
  return f;
}

int fread(void *ptr, int size, int count, FILE *stream)
{
  return fat_read(&fatfs, stream, ptr, size * count);
}

int fgetc(FILE *stream)
{
  return fat_getc(&fatfs, stream);
}

int fseek(FILE *stream, int offset, int origin)
{
  fatfs_file_t *f = stream;
  if(origin == SEEK_CUR) offset += f->pos;
  else if(origin == SEEK_END) offset = f->size - offset;
  return fat_seek(&fatfs, stream, offset);
}

int ftell(FILE *stream)
{
  fatfs_file_t *f = stream;
  return f->pos;
}

int fclose(FILE *stream)
{
  if(stream) free(stream);
  return 0;
}

