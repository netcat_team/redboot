/*
 * mbr.h
 *
 *  Created on: Apr 8, 2017
 *      Author: netcat
 */

#ifndef MBR_H
#define MBR_H

#include "hw_defs.h"

#define MBR_SIGNATURE 0xaa55
#define MBR_PARTS_MAX 4
#define MBR_SIZE 512

enum
{
  MBR_PART_TYPE_EXFAT = 0x07,
  MBR_PART_TYPE_FAT16 = 0x06,
  MBR_PART_TYPE_FAT32 = 0x0b,
  MBR_PART_TYPE_LINUX_SWAP = 0x82,
  MBR_PART_TYPE_LINUX = 0x83,
  MBR_PART_TYPE_LINUX_EXTENDED = 0x85,
};

// partition entry
typedef struct __attribute__ ((packed))
{
 uint8_t status;
 uint8_t chs[3];
 uint8_t type;
 uint8_t chs_last[3];
 uint32_t start;
 uint32_t count;
} mbr_pe_t;

// generic MBR
typedef struct __attribute__ ((packed))
{
  uint8_t bl[446];
  mbr_pe_t part[4];
  uint16_t sign;
} mbr_t;

uint32_t mbr_find_part(io_blk_t *io, void (*cb)(uint32_t addr, uint8_t type));

#endif
