/*
 * mbr.c
 *
 *  Created on: Apr 8, 2017
 *      Author: netcat
 */

#include "mbr.h"
#include "syslog.h"

uint32_t mbr_find_part(io_blk_t *io, void (*cb)(uint32_t addr, uint8_t type))
{
  uint32_t found = 0;
  mbr_t *mbr = malloc(sizeof(mbr_t));
  if(mbr)
  {
    if(io->read(mbr, 0, sizeof(mbr_t)))
    {
      syslog(SYSLOG_ERR, "[mbr] io error");
      return 0;
    }

    if(mbr->sign != MBR_SIGNATURE)
    {
      syslog(SYSLOG_ERR, "[mbr] wrong sign: 0x%04x");
      return 1;
    }

    uint32_t i;
    for(i = 0; i < MBR_PARTS_MAX; i++)
    {
      uint8_t type = mbr->part[i].type;
      if(type)
      {
        found++;
        syslog(SYSLOG_DEBUG, "[mbr] part: %d, type: 0x%02x, st: 0x%02x, at: 0x%x",
            i, type, mbr->part[i].status, mbr->part[i].start);
        cb(mbr->part[i].start, type);
      }
    }
    free(mbr);
  }
  return found;
}
