/*
 * fat.c
 *
 *  Created on: Apr 8, 2017
 *      Author: netcat
 */
#include <string.h>

#include "syslog.h"
#include "fatfs.h"

void fat_cluster(fatfs_t *fs, void *buf, uint32_t cluster)
{
  fs->io->read(buf, fs->data_addr + cluster - 2, FAT_CLUSTER_SIZE);
}

uint32_t fat_table(fatfs_t *fs, void *buf, uint32_t cluster)
{
  uint32_t b_offset, s_offset;

  cluster &= 0x0fffffff;
  s_offset = (cluster >> 7) + fs->fs_addr;
  b_offset = (cluster & 0x7f) << 2;

  fs->io->read(buf, s_offset, FAT_CLUSTER_SIZE);

  return *((uint32_t*)(buf + b_offset));
}

static void utf16_raw(void *buf, uint32_t cnt)
{
  uint8_t *b8 = buf;
  uint16_t *b16 = buf;
  cnt >>= 1;
  while(cnt--) *++b8 = *++b16;
  *b8 = 0;
}

static fat_dir_t *fat_find(fatfs_t *fs, void *buf, const char *name, uint32_t name_len, uint32_t addr)
{
  uint32_t it = 0;
  fat_dir_t *p = buf;

  fat_cluster(fs, buf, addr);
  fs->lnf_len = 0;

  while(1)
  {
    uint8_t attr = p[it].attributes;
    uint8_t n0 = p[it].name[0];

    if(n0 == FAT_FILE_ERASED || n0 == FAT_FILE_PENDING_ERASE) {}
    else if(n0 == FAT_FILE_EMPTY) {}
    else if(attr == FILE_ATTR_VOLUME_ID) {}
    else if(attr == FILE_ATTR_LONG_NAME)
    {
      fat_long_name_t *t = (void *)&p[it];
      if(t->id & 0x40) fs->lnf_len = 0;
      uint8_t *p = fs->lnf_buf + (26 * ((t->id & 0xf) - 1));

      memcpy(p, t->n1, 10);
      memcpy(p + 10, t->n2, 12);
      memcpy(p + 22, t->n3, 4);
      fs->lnf_len += 26;
    }
    else
    {
      if(!fs->lnf_len)
      {
        it++;
        continue;
      }

      utf16_raw(fs->lnf_buf, fs->lnf_len);
//      printf("    %s \n", fs->lnf_buf);

      if(!strncmp(name, (const char*)fs->lnf_buf, name_len)) return &p[it];
    }

    if(++it >= fs->sector_in_cluster * (FAT_CLUSTER_SIZE / sizeof(fat_dir_t)))
    {
      uint32_t next = fat_table(fs, buf, addr);
      if(next != FAT_EOC1 && next != FAT_EOC2)
      {
        it = 0;
        addr = next;
        fat_cluster(fs, buf, addr);
      }
      else break;
    }
  }
  return 0;
}

fat_dir_t *fat_find_path(fatfs_t *fs, const char *path)
{
  uint32_t i = 0, st = 0;
  fat_dir_t *dir = 0;
  uint32_t addr = FAT_ROOT_CLUSTER;

  if(path[0] == '/') { st++; i++; }

  for(;; i++)
  {
    uint8_t s = path[i];
    if(s == '/' || !s)
    {
      dir = fat_find(fs, fs->buf, &path[st], i - st, addr);
      if(!dir) break;
      addr = dir->first_cluster_lo | (dir->first_cluster_hi << 16);

      if(!s) break;
      st = i + 1;
    }
  }
  return dir;
}

uint32_t fat_init(fatfs_t *fs, uint32_t addr)
{
  // already initialized
  if(fs->fs_addr) return 1;
  fs->fs_addr = addr;

  if(fs->io->read(fs->buf, addr, FAT_CLUSTER_SIZE))
  {
    syslog(SYSLOG_ERR, "[fat] io error");
    fs->fs_addr = 0;
    fs->data_addr = 0;
    return 1;
  }

  fat_bpb_t *fat_bpb = (fat_bpb_t *)fs->buf;
  fat_bpb32_t *fat_bpb32 = (fat_bpb32_t *)(fs->buf + sizeof(fat_bpb_t));
  fs->fs_addr += fat_bpb->reserved_sectors;
  fs->sector_in_cluster = fat_bpb->sector_in_cluster;

  if(fat_bpb->sector_in_cluster != 1)
  {
    syslog(SYSLOG_ERR, "[fat] sector_in_cluster: %d", fat_bpb->sector_in_cluster);
    fs->fs_addr = 0;
    fs->data_addr = 0;
    return 1;
  }
  fs->data_addr = fs->fs_addr + fat_bpb->num_fats * fat_bpb32->fat32_sectors;

  syslog(SYSLOG_DEBUG, "[fat] init at 0x%x done", addr);
  return 0;
}

// ---------- file io section ----------

fatfs_file_t *fat_open(fatfs_t *fs, fatfs_file_t *file, const char *path)
{
  fat_dir_t *dir = fat_find_path(fs, path);
  if(dir)
  {
    file->addr = dir->first_cluster_lo | (dir->first_cluster_hi << 16);
    file->buf_addr = file->addr;
    file->pos = 0;
    file->size = dir->file_size - 1;

    fat_cluster(fs, file->buf, file->addr);

    return file;
  }

  return 0;
}

uint32_t fat_seek(fatfs_t *fs, fatfs_file_t *file, uint32_t len)
{
  if(!file) return 0;

  file->pos = 0;
  if(len > file->size) len = file->size;

  uint32_t addr = file->addr;
  while(len > FAT_CLUSTER_SIZE)
  {
    uint32_t t = fat_table(fs, file->buf, addr);
    if(t == FAT_EOC1 && t == FAT_EOC2)
    {
      len = 0;
      break;
    }
    addr = t;
    len -= FAT_CLUSTER_SIZE;
    file->pos += FAT_CLUSTER_SIZE;
  }
  file->pos += len & (FAT_CLUSTER_SIZE - 1);
  file->buf_addr = addr;
  fat_cluster(fs, file->buf, file->buf_addr);

  return 0;
}

uint32_t fat_read(fatfs_t *fs, fatfs_file_t *file, uint8_t *buf, uint32_t size)
{
  uint32_t left = file->size - file->pos;
  uint32_t rd = 0;

  if(!file) return 0;

  if(size > left) size = left;
  while(size)
  {
    int pos_per_sector = file->pos & (FAT_CLUSTER_SIZE - 1);
    int left_per_sector = FAT_CLUSTER_SIZE - pos_per_sector;

    if(size < left_per_sector)
    {
      memcpy(&buf[rd], file->buf + pos_per_sector, size);
      file->pos += size;

      rd += size;
      break;
    }
    else
    {
      memcpy(&buf[rd], file->buf + pos_per_sector, left_per_sector);
      file->buf_addr = fat_table(fs, file->buf, file->buf_addr);
      fat_cluster(fs, file->buf, file->buf_addr);

      rd += left_per_sector;
      size -= left_per_sector;
      file->pos += left_per_sector;
    }
  }
  return rd;
}

int32_t fat_getc(fatfs_t *fs, fatfs_file_t *file)
{
  if(file->size - file->pos)
  {
    int sec = file->pos++ & (FAT_CLUSTER_SIZE - 1);
    if(sec >= FAT_CLUSTER_SIZE - 2)
    {
      file->buf_addr = fat_table(fs, file->buf, file->buf_addr);
      fat_cluster(fs, file->buf, file->buf_addr);
    }
    return file->buf[sec];
  }
  return FAT_EOCF;
}

