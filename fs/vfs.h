/*
 * vfs.h
 *
 *  Created on: Apr 27, 2020
 *      Author: netcat
 */

#ifndef VFS_H_
#define VFS_H_

#include "hw_defs.h"

#ifndef SEEK_SET
enum
{
  SEEK_SET,
  SEEK_CUR,
  SEEK_END
};
#endif

#ifndef EOF
#define EOF -1
#endif

typedef void FILE;

FILE *fopen(const char *filename, const char *mode);
int fread(void *ptr, int size, int count, FILE *stream);
int fseek(FILE *stream, int offset, int origin);
int ftell(FILE *stream);
int fgetc(FILE *stream);
int fclose(FILE *stream);

void vfs_init(void);

#endif /* VFS_H_ */
