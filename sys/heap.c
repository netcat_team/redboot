/*
 * heap.c
 *
 *  Created on: Jul 22, 2019
 *      Author: netcat
 */

#include <stdint.h>
#include <stddef.h>
#include "sys/heap.h"

static heap_block_t *heap_create_block(heap_block_t *prev, uint32_t size)
{
  heap_block_t *ret = (heap_block_t *)(((uint8_t *)prev) + size);
  ret->size = prev->size - size;
  prev->size = size;

  return ret;
}

static void heap_free_block(heap_block_t *it, heap_header_t *heap, heap_block_t *block)
{
  if(!it)
  {
    for(it = heap->first; it->next < block; it = it->next);
  }

  // split left
  if((((uint8_t *)it) + it->size) == (uint8_t *)block)
  {
    it->size += block->size;
    block = it;
  }

  // split right
  if((((uint8_t *)block) + block->size) == ( uint8_t * )it->next)
  {
    if(it->next != heap->last )
    {
      block->size += it->next->size;
      block->next = it->next->next;
    }
    else block->next = heap->last;
  }
  else block->next = it->next;

  if(it != block) it->next = block;
}

void *heap_alloc(heap_header_t *heap, uint32_t size)
{
  if(!size) return 0;

  size += HEAP_ALIGN_ADDR(size);
  size += sizeof(heap_block_t);

  if(size <= heap->remaining)
  {
    heap_block_t *prev, *it;

    // find block with needed space
    prev = heap->first;
    it = heap->first->next;
    while(it->size < size && it->next)
    {
      prev = it;
      it = it->next;
    }
    if(it == heap->last) return 0;

    // remove bind
    prev->next = it->next;
    it->next = 0;

    // is enough space for one more block?
    if(it->size - size > sizeof(heap_block_t))
      heap_free_block(prev, heap, heap_create_block(it, size));

    heap->remaining -= it->size;
    return ((uint8_t *)it) + sizeof(heap_block_t);
  }
  return 0;
}

void heap_free(heap_header_t *heap, void *ptr)
{
  if(!ptr) return;
  heap_block_t *it = (heap_block_t *)(((uint8_t *)ptr) - sizeof(heap_block_t));

  heap->remaining += it->size;
  heap_free_block(0, heap, it);
}

void heap_conf(heap_header_t *heap, uint32_t addr, uint32_t size)
{
  heap->addr = addr;
  heap->size = size;

  uint32_t t = HEAP_ALIGN_ADDR(addr);
  if(t)
  {
    addr += t;
    size -= t;
  }

  heap->remaining = size - sizeof(heap_block_t) * 2;

  heap->first = (heap_block_t *)addr;
  heap->first->next = (heap_block_t *)(addr + sizeof(heap_block_t));
  heap->first->size = 0;

  heap->last = (heap_block_t *)(addr + size - sizeof(heap_block_t));
  heap->last->next = 0;
  heap->last->size = 0;

  heap_block_t *first = heap->first->next;
  first->next = heap->last;
  first->size = heap->remaining;
}

