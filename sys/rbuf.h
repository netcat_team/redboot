/*
 * rbuf.h
 *
 *  Created on: Apr 24, 2020
 *      Author: netcat
 */

#ifndef RBUF_H
#define RBUF_H

typedef struct
{
  char *data;
  size_t size;
  volatile size_t head;
  volatile size_t tail;
} rbuf_t;

void rbuf_init(rbuf_t *rb, int size);
void rbuf_destroy(rbuf_t *rb);
void rbuf_clear(rbuf_t *rb);
int rbuf_count(const rbuf_t *rb);
int rbuf_capacity(const rbuf_t *rb);
int rbuf_put(rbuf_t *rb, uint8_t s);
int rbuf_get(rbuf_t *rb, uint8_t *s);

#endif /* RBUF_H */
