/*
 * syslog.c
 *
 *  Created on: Nov 2, 2018
 *      Author: netcat
 */

#include "sys/cli/syslog.h"
#include "sys/cli/printf.h"
#include "hal/hw_uart.h"
#include <stdarg.h>

static void vsyslog(uint32_t pri, const char *fmt, va_list args)
{
  char last_syslog_msg[MAX_SYSLOG_MESSAGE];
  vsnprintf(last_syslog_msg, sizeof(last_syslog_msg), fmt, args);

  const char *color_str = 0;
  if(pri <= SYSLOG_ERR) color_str = ESC_CLR_RED;
  else if(pri <= SYSLOG_NOTICE) color_str = ESC_CLR_YELLOW;
  else if(pri == SYSLOG_DEBUG) color_str = ESC_BCLR_BLACK;
  else color_str = ESC_CLR_RESET;

  printf("%s%s%s\n", color_str, last_syslog_msg, ESC_CLR_RESET);
}

void syslog(uint32_t pri, const char *fmt, ...)
{
  va_list args;
  va_start(args, fmt);
  vsyslog(pri, fmt, args);
  va_end(args);
}

void slog(const char * fmt, ...)
{
  va_list args;
  va_start(args, fmt);
  vsyslog(SYSLOG_INFO, fmt, args);
  va_end(args);
}

