/*
 * printf.c
 *
 *  Created on: May 5, 2020
 *      Author: netcat
 */

#include <string.h>
#include "printf.h"

char *utoa_fast(uint32_t n, char *buf, int base)
{
  if(!n)
  {
    *--buf = '0';
    return buf;
  }

  if(base == 10)
  {
    while(n)
    {
      // umul ~0.8
      uint32_t i = n >> 1;
      i += i >> 1;
      i += i >> 4;
      i += i >> 8;
      i += i >> 16;
      // div 8
      i >>= 3;

      // remain
      uint8_t rem = n - ((i << 1) + (i << 3));
      // correction
      if(rem > 9)
      {
        rem -= 10;
        i++;
      }

      n = i;
      *--buf = rem + '0';
    }
  }
  else if(base == 16)
  {
    while(1)
    {
      uint8_t i = n & 0xf;
      n >>= 4;
      if(!n && !i) break;
      *--buf = "0123456789abcdef"[i];
    }
  }
  else if(base == 2)
  {
    while(1)
    {
      uint8_t i = n & 1;
      n >>= 1;
      if(!n && !i) break;
      *--buf = i + '0';
    }
  }

  return buf;
}

void prt(const char *s, v_ctx_t *ctx)
{
  // size
  int l = strlen(s);
  if(ctx->flags & FLAGS_PRECISION)
    l = l < ctx->precision ? l : ctx->precision; // min macro?

  // string + borders
  if(!(ctx->flags & FLAGS_MINUS))
  {
    char c = ctx->flags & FLAGS_ZERO ? '0' : ' ';
    while(l++ < ctx->width) ctx->out(c, ctx);
  }
  while((*s != 0) && (!(ctx->flags & FLAGS_PRECISION) || ctx->precision--)) ctx->out(*s++, ctx);
  if(ctx->flags & FLAGS_MINUS)
  {
    while(l++ < ctx->width) ctx->out(' ', ctx);
  }
}

static int _vsnprintf(v_ctx_t *ctx, const char *fmt, va_list args)
{
  // %[flags][width][.precision][length]specifier
  while(*fmt)
  {
    // find %
    char c = *fmt++;
    if(c != '%')
    {
      ctx->out(c, ctx);
      continue;
    }

    // printf("\n c: [%c] ", *fmt);

    // flags
    ctx->flags = 0;
    while(1)
    {
      c = *fmt;
      if(c == '-') ctx->flags |= FLAGS_MINUS;
      else if(c == '+') ctx->flags |= FLAGS_PLUS;
      else if(c == ' ') ctx->flags |= FLAGS_SPACE;
      else if(c == '#') ctx->flags |= FLAGS_HASH;
      else if(c == '0') ctx->flags |= FLAGS_ZERO;
      else break;
      fmt++;
    }

    // width
    int w = 0;
    while(1)
    {
      if(!isdigit(*fmt))
      {
        ctx->width = w;
        // printf("w: [%d] ", width);
        break;
      }
      w *= 10;
      w += *fmt - '0';
      fmt++;
    }

    if(*fmt == '*')
    {
      w = va_arg(args, int);
      if(w < 0)
      {
        ctx->width = -w;
        ctx->flags |= FLAGS_MINUS;
      }
      else ctx->width = w;
      fmt++;
      // printf("w*: [%d] ", width);
    }

    // precision
    if(*fmt == '.')
    {
      fmt++;

      int p = 0;
      while(1)
      {
        if(!isdigit(*fmt))
        {
          ctx->flags |= FLAGS_PRECISION;
          ctx->precision = p;
          // printf("p: [%d] ", precision);
          break;
        }
        p *= 10;
        p += *fmt - '0';
        fmt++;
      }
      if(*fmt == '*')
      {
        p = va_arg(args, int);
        ctx->precision = p > 0 ? p : 0;
        fmt++;
        // printf("p*: [%d] ", precision);
      }
    }

    // length
    c = *fmt;
    if(*fmt == 'l')
    {
      if(*++fmt != 'l') ctx->flags |= FLAGS_LONG;
      else
      {
        fmt++;
        ctx->flags |= FLAGS_LONG_LONG;
      }
    }
    else if(*fmt == 'h')
    {
      if(*++fmt != 'h') ctx->flags |= FLAGS_SHORT;
      else
      {
        fmt++;
        ctx->flags |= FLAGS_SHORT_SHORT;
      }
    }
    // just ignore
    else if(c == 'j' || c == 'z' || c == 't' || c == 'L') fmt++;

    // specifier
    c = *fmt;
    switch(c)
    {
      case 's':
      {
        char *s = va_arg(args, char *);
        ctx->flags &= ~FLAGS_ZERO;
        prt(s, ctx);
      }
      break;

      case 'c':
      {
        char c = va_arg(args, int);
        char s[] = { c, 0 };
        ctx->flags &= ~FLAGS_ZERO;
        prt(s, ctx);
      }
      break;

      case 'd':
      case 'i':
      case 'u':
      case 'x':
      case 'X':
      case 'o':
      case 'b':
      {
        int i = va_arg(args, int);

        int base = 10;
        if(c == 'x' || c == 'X') base = 16;
        else if((c == 'd' || c == 'i') && i < 0)
        {
          i = 0 - i;
          ctx->flags |= FLAGS_SIGN;
          ctx->flags &= ~(FLAGS_PLUS | FLAGS_SPACE);
        }
        else if(c == 'b') base = 2;

        char buf[16];
        char *b = buf + sizeof(buf) - 1;
        *b = 0;

        char *s = utoa_fast(i, b, base);
        if(ctx->flags & FLAGS_SIGN) *--s = '-';

        prt(s, ctx);
      }
      break;

      case '%':
        ctx->out('%', ctx);
        break;

      default:
        continue;
    }
    fmt++;
  }
  ctx->out(0, ctx);

  return ctx->cnt;
}

static inline void ctx_buf(char c, v_ctx_t *ctx)
{
  if(ctx->cnt < ctx->maxlen)
    ctx->buf[ctx->cnt++] = c;
}

int snprintf(char *s, size_t maxlen, const char *fmt, ...)
{
  va_list args;
  va_start(args, fmt);
  v_ctx_t ctx = { .buf = s, .maxlen = maxlen, .out = ctx_buf };
  int ret = _vsnprintf(&ctx, fmt, args);

  va_end(args);
  return ret;
}

int vsnprintf(char *s, size_t maxlen, const char *fmt, va_list args)
{
  v_ctx_t ctx = { .buf = s, .maxlen = maxlen, .out = ctx_buf };
  return _vsnprintf(&ctx, fmt, args);
}

void bsp_cli(char);

int printf(const char* fmt, ...)
{
  va_list args;
  va_start(args, fmt);
  v_ctx_t ctx = { .out = (vp_fp)bsp_cli };
  int ret = _vsnprintf(&ctx, fmt, args);

  va_end(args);
  return ret;
}
