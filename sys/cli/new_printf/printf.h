/*
 * printf.h
 *
 *  Created on: May 5, 2020
 *      Author: netcat
 */

#ifndef _PRINTF_H_
#define _PRINTF_H_

#include <stdint.h>
#include <stdarg.h>

#define printf    printf_
#define sprintf   sprintf_
#define snprintf  snprintf_
#define vsnprintf vsnprintf_

typedef struct v_ctx_s
{
  uint32_t flags;
  uint32_t width;
  uint32_t precision;
  uint32_t cnt;
  void (*out)(char c, struct v_ctx_s *ctx);
  char *buf;
  const uint32_t maxlen;
} v_ctx_t;

typedef void (*vfp)(char c, v_ctx_t *ctx);

enum
{
  FLAGS_SIGN = 1,
  FLAGS_MINUS = 2,
  FLAGS_PLUS = 4,
  FLAGS_ZERO = 8,
  FLAGS_SPACE = 16,
  FLAGS_HASH = 32,
  FLAGS_PRECISION = 64,
  FLAGS_LONG = 128,
  FLAGS_LONG_LONG = 256,
  FLAGS_SHORT = 512,
  FLAGS_SHORT_SHORT = 1024,
};

int snprintf(char *s, uint32_t maxlen, const char *fmt, ...);
int vsnprintf(char *s, uint32_t maxlen, const char *fmt, va_list args);
int printf(const char* fmt, ...);

#endif  // _PRINTF_H_
