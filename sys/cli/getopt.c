#include <ctype.h>
#include <stdio.h>
#include <string.h>

#include "getopt.h"

int htoi(const char *str)
{
  int len = strlen(str);
  int ret = 0;

  for(int i = 0; i < len; i++)
  {
    char c = str[i];
    ret <<= 4;

    if(c >= '0' && c <= '9') ret |= c - '0';
    else if(c >= 'A' && c <= 'F') ret |= (c - 'A') + 10;
  }

  return ret;
}

void getopt_init(getopt_ctx_t *ctx)
{
  ctx->ind = 1;
  ctx->opt = 0;
  ctx->arg = 0;
  ctx->err = 0;
}

int getopt(getopt_ctx_t *ctx, int argc, char *argv[], const char *optstring)
{
  int pos = 1;
  char *arg = argv[ctx->ind];

  if(ctx->ind > argc) ctx->err = 0;
  else if(arg && strcmp(arg, "--") == 0) ctx->err = 0;
  else if(!arg || arg[0] != '-') ctx->err = 0; // || !isalnum(arg[1])
  else
  {
    const char *opt = strchr(optstring, arg[pos]);
    ctx->opt = arg[pos];
    if(!opt)
    {
      ctx->ind++;
      ctx->err = '?';
    }
    else if(opt[1] == ':')
    {
      if(arg[pos + 1])
      {
        ctx->arg = arg + pos + 1;
        ctx->ind++;
        pos = 1;
        ctx->err = ctx->opt;
      }
      else if(argv[ctx->ind + 1])
      {
        ctx->arg = argv[ctx->ind + 1];
        if(ctx->arg[0] != '-')
        {
          ctx->ind += 2;
          pos = 1;
          ctx->err = ctx->opt;
        }
        else
        {
          ctx->ind++;
          ctx->arg = argv[ctx->ind];
          ctx->err = ':';
        }
      }
      else
      {
        ctx->ind++;
        ctx->arg = argv[ctx->ind];
        ctx->err = ':';
      }
    }
    else
    {
      if(!arg[++pos])
      {
        ctx->ind++;
        pos = 1;
      }
      ctx->err = ctx->opt;
    }
  }
  return ctx->err;
}

