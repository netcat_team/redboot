/*
 * vt.c
 *
 *  Created on: Jan 08, 2020
 *      Author: vchumakov
 */

#include <string.h>
//#include <ctype.h>
#include <stdio.h>

#include "sys/cli/printf.h"
#include "vt.h"
#include "syslog.h"

static void vt_new_line(vt_t *vt, uint32_t exec_flag)
{
  vt->out(VT_LINE_END, vt);

  if(exec_flag && vt->exec != NULL && vt->cmdlen > 0)
    vt->exec(vt->cmdline);

  vt->out(vt->promt, vt);
  vt->cmdlen = 0;
  vt->cursor = 0;
}

static void vt_move(vt_t *vt, int offset)
{
  char str[16];
  if(offset > 0)
  {
    snprintf(str, sizeof(str), "\x1b[%uC", offset);
    vt->out(str, vt);
  }
  else if(offset < 0)
  {
    snprintf(str, sizeof(str), "\x1b[%uD", -offset);
    vt->out(str, vt);
  }
}

static void vt_reset(vt_t *vt)
{
  char str[16];
  snprintf(str, sizeof(str), "\x1b[%dD\x1b[%dC", (int)(VT_CMD_LINE_LEN + vt->promt_len + 2), (int)(vt->promt_len));
  vt->out(str, vt);
}

static void vt_line(vt_t *vt, int pos)
{
  // delete all from cursor
  vt->out("\x1b[K", vt);
  vt->out(&vt->cmdline[pos], vt);
  vt_reset(vt);
  vt_move(vt, vt->cursor);
}

static uint32_t vt_insert(vt_t *vt, uint8_t rx)
{
  if(vt->cmdlen + 1 < VT_CMD_LINE_LEN)
  {
    memmove(vt->cmdline + vt->cursor + 1, vt->cmdline + vt->cursor, vt->cmdlen - vt->cursor);
    vt->cmdline[vt->cursor] = rx;
    vt->cursor++;
    vt->cmdlen++;
    vt->cmdline[vt->cmdlen] = 0;
    return 1;
  }
  return 0;
}

static void vt_rm(vt_t *vt, uint32_t left)
{
  if(left && vt->cursor > 0)
  {
    vt->out("\x1b[D", vt); // left one symbol
    char *p = vt->cmdline + vt->cursor;
    memmove(p - 1, p, vt->cmdlen - vt->cursor + 1);
    vt->cursor--;
    vt->cmdline[vt->cmdlen] = 0;
    vt->cmdlen--;

    vt_line(vt, vt->cursor);
  }
  else if(!left && vt->cursor < vt->cmdlen)
  {
    char *p = vt->cmdline + vt->cursor;
    memmove(p, p + 1, vt->cmdlen - vt->cursor + 1);
    vt->cmdline[vt->cmdlen] = 0;
    vt->cmdlen--;

    vt_line(vt, vt->cursor);
  }
}


void vt_telnet_opt(vt_t *vt, uint8_t ctrl, uint8_t opt)
{
  const char str[] = { VT_TELNET_IAC, ctrl, opt, 0 };
  vt->out(str, vt);
}

void vt_init(vt_t *vt, int (*exec)(char *), vt_fp fp, int sock, const char *promt)
{
  vt->esc = 0;
  vt->esc_digit = 0;
  vt->cursor = 0;
  vt->cmdlen = 0;
  vt->promt = promt;
  vt->promt_len = strlen(promt);
  vt->exec = exec;
  vt->out = fp;
  vt->sock = sock;
  vt->opt = 0;

  char init_str[] = {
      VT_TELNET_IAC, VT_TELNET_WILL, 1,
      VT_TELNET_IAC, VT_TELNET_WILL, 3,
      VT_TELNET_IAC, VT_TELNET_DONT, 34,
      0 };
  vt->out(init_str, vt);

  vt->out(vt->promt, vt);
}

static void vt_telnet_ctrl(vt_t *vt, int ctrl, int opt)
{
  if(ctrl == VT_TELNET_WILL)
  {
    if(opt == TELNET_OPT_SGA) vt_telnet_opt(vt, VT_TELNET_DO, opt);
    else vt_telnet_opt(vt, VT_TELNET_DONT, opt);
  }
  else if(ctrl == VT_TELNET_DO)
  {
    if(opt == TELNET_OPT_ECHO ||
       opt == TELNET_OPT_SGA) vt_telnet_opt(vt, VT_TELNET_WILL, opt);
    else vt_telnet_opt(vt, VT_TELNET_WONT, opt);
  }
}

void vt_input(vt_t *vt, uint8_t rx)
{
//  syslog(SYSLOG_WARNING, "vt", "%02x (%c)", rx, isalpha(rx) ? rx : '?');

  if(vt->esc == ESC_NONE) // non ESC codes
  {
    if(rx == VT_TELNET_IAC) vt->esc = TELNET_IAC;
    else if(!(vt->opt & VT_SKIP))
    {
      switch(rx)
      {
        case VT_KEY_ESC:
          vt->esc = ESC_BEGIN;
          break;

        case VT_KEY_ETX:
        case VT_KEY_EOT:
          vt_new_line(vt, 0);
          break;

        case VT_KEY_CR:
          break;

        case VT_KEY_DEL:
        case VT_KEY_BS:
          vt_rm(vt, 1);
          break;

        case VT_KEY_NUL: // linux terminal
        case VT_KEY_LF:
          vt_new_line(vt, 1);
          break;

        default:
          if(rx > VT_KEY_US && vt_insert(vt, rx)) vt_line(vt, vt->cursor - 1);
      }
    }
  }
  else if(vt->esc == ESC_BEGIN) // ESC + ...
  {
    if(rx == '[')
    {
      vt->esc = ESC_LEFT_BRACKET;
      vt->esc_digit = 0;
    }
    else if(rx == 'O')
    {
      vt->esc = ESC_O;
      vt->esc_digit = 0;
    }
    else
    {
      vt->esc = ESC_NONE;
    }
  }
  else if(vt->esc == ESC_LEFT_BRACKET) // ESC + [ + ...
  {
    if(isdigit(rx)) // collect numbers as hex
    {
      vt->esc_digit = (vt->esc_digit << 4) | (rx - '0');
      return;
    }
    switch(rx)
    {
      case 'A': // hist prev
      case 'B': // hist next
        break;

      case 'C': // RIGHT
        if(vt->cursor < vt->cmdlen)
        {
          vt_move(vt, 1);
          vt->cursor++;
        }
        break;

      case 'D': // LEFT
        if(vt->cursor)
        {
          vt_move(vt, -1);
          vt->cursor--;
        }
        break;

      case '~':
        if(vt->esc_digit == 1) // Home
        {
          vt_move(vt, -vt->cursor);
          vt->cursor = 0;
        }
        else if(vt->esc_digit == 3) vt_rm(vt, 0); // DEL
        break;

      // ESC + [ + US/; - ignoring
      case VT_KEY_US:
      case ';':
        return;

      default:
        break;
    }
    vt->esc = ESC_NONE;
  }
  else if(vt->esc == ESC_O) // ESC + O + ...
  {
    if(rx == 'F') // End
    {
      vt_move(vt, vt->cmdlen - vt->cursor);
      vt->cursor = vt->cmdlen;
    }
    vt->esc = ESC_NONE;
  }
  // telnet
  else if(vt->esc == TELNET_IAC)
  {
    vt->telnet_ctrl = rx;

    if(rx >= VT_TELNET_WILL && rx <= VT_TELNET_DONT)
    {
      vt->esc = TELNET_CMD;
      return;
    }
    else if(rx == VT_TELNET_SB) vt->opt |= VT_SKIP; // long opt start
    else if(rx == VT_TELNET_SE) vt->opt &= ~VT_SKIP; // long opt end

    vt->esc = ESC_NONE;
  }
  else if(vt->esc == TELNET_CMD)
  {
//    vt_telnet_dbg(vt, vt->telnet_ctrl, rx, 0);
    vt_telnet_ctrl(vt, vt->telnet_ctrl, rx);
    vt->esc = ESC_NONE;
  }
}
