/*
 * apps.c
 *
 *  Created on: Jun 30, 2019
 *      Author: netcat
 */

#include <stdlib.h>
#include <string.h>

#include "printf.h"
#include "apps.h"
#include "syslog.h"
#include "vt.h"

// ld
extern int __apps_db_start;
extern int __apps_db_end;

apps_table_st *apps_db;
int apps_db_cnt = 0;

static int apps_split_cmd_args(char *buf, char **argv)
{
  int i = 0;
  int cnt = 0;
  int f = 1;

  while(buf[i])
  {
    if(f && buf[i] != ' ')
    {
      argv[cnt++] = &buf[i];
      if(cnt > APP_ARGS_MAX) return cnt;
      f = 0;
    }
    if(buf[i] == ' ')
    {
      buf[i] = 0;
      f = 1;
    }
    i++;
  }
  return cnt;
}

apps_table_st *apps_find(char *str)
{
  int cnt = apps_db_cnt;
  apps_table_st *app = apps_db;
  while(cnt--)
  {
    if(!strcmp(app->str, str)) return app;
    app++;
  }
  return 0;
}

int apps_run(char *buf)
{
  char *argv[APP_ARGS_MAX];
  int32_t argc = apps_split_cmd_args(buf, argv);
  if(!argc) return 1;

  if(!strcmp(argv[0], "?")) apps_list();
  else
  {
    apps_table_st *app = apps_find(argv[0]);
    if(app) return app->cli(argc, argv);

    printf("command not found '%s' \n", argv[0]);
    return 1;
  }

  return 0;
}

void apps_list()
{
  int cnt = apps_db_cnt;
  apps_table_st *app = apps_db;
  while(cnt--)
  {
    printf("%s \n", app->str);
    app++;
  }
}

int apps_cmp(const void *a, const void *b)
{
  return strcmp(((apps_table_st *)a)->str, ((apps_table_st *)b)->str);
}

void apps_init()
{
  int size = (int)&__apps_db_end - (int)&__apps_db_start;
  apps_db_cnt = size / (sizeof(apps_table_st));

#if 1
  apps_db = malloc(size);
  memcpy(apps_db, &__apps_db_start, size);
  qsort(apps_db, apps_db_cnt, sizeof(apps_table_st), apps_cmp);
#else
  apps_db = (apps_table_st*)&__apps_db_start;
#endif

  syslog(SYSLOG_DEBUG, "apps: %d", apps_db_cnt);
}


