/*
 * vt.h
 *
 *  Created on: Jan 08, 2020
 *      Author: vchumakov
 */

#ifndef APPS_VT_H_
#define APPS_VT_H_

#include <stdint.h>

#define VT_CMD_LINE_LEN 128
#define VT_LINE_END "\r\n"

typedef struct vt_s
{
  uint32_t esc;
  uint32_t esc_digit;
  uint32_t cursor;
  uint32_t cmdlen;
  char cmdline[VT_CMD_LINE_LEN];
  const char *promt;
  uint32_t promt_len;
  int (*exec)(char *buf);
  void (*out)(const char *str, struct vt_s *vt);
  int sock;
  uint32_t opt;
  uint32_t telnet_ctrl;
} vt_t;

typedef void (*vt_fp)(const char *str, struct vt_s *vt);

#define VT_ECHO 1
#define VT_SKIP 2

enum
{
  ESC_NONE,
  ESC_BEGIN,
  ESC_LEFT_BRACKET,
  ESC_O,
  TELNET_IAC,
  TELNET_CMD,
};

enum
{
  TELNET_OPT_ECHO = 1,
  TELNET_OPT_SGA = 3,
  TELNET_OPT_TM = 6, // CTRL+C
  TELNET_OPT_TTYPE = 24,
  TELNET_OPT_NAWS = 31,
  TELNET_OPT_TSPEED = 32,
  TELNET_OPT_LFLOW = 33,
  TELNET_OPT_LINEMODE = 34,
  TELNET_OPT_XDISPLOC = 35,
  TELNET_OPT_NEW_ENNVIRON = 39,
};

enum
{
  VT_KEY_NUL = 0,
  VT_KEY_SOH = 1,
  VT_KEY_STX = 2,
  VT_KEY_ETX = 3,
  VT_KEY_EOT = 4,
  VT_KEY_ENQ = 5,
  VT_KEY_ACK = 6,
  VT_KEY_BEL = 7,
  VT_KEY_BS = 8,
  VT_KEY_HT = 9,
  VT_KEY_LF = 10,
  VT_KEY_VT = 11,
  VT_KEY_FF = 12,
  VT_KEY_CR = 13,
  VT_KEY_SO = 14,
  VT_KEY_SI = 15,
  VT_KEY_DLE = 16,
  VT_KEY_DC1 = 17,
  VT_KEY_DC2 = 18,
  VT_KEY_DC3 = 19,
  VT_KEY_DC4 = 20,
  VT_KEY_NAK = 21,
  VT_KEY_SYN = 22,
  VT_KEY_ETB = 23,
  VT_KEY_CAN = 24,
  VT_KEY_EM = 25,
  VT_KEY_SUB = 26,
  VT_KEY_ESC = 27, // 1b
  VT_KEY_FS = 28,
  VT_KEY_GS = 29,
  VT_KEY_RS = 30,
  VT_KEY_US = 31, // 1f
  VT_KEY_DEL = 127, // 7f

  VT_TELNET_SE = 240, // F0
  VT_TELNET_IP = 244, // F4
  VT_TELNET_EL = 248, // F8
  VT_TELNET_SB = 250, // FA
  VT_TELNET_WILL = 251, // FB
  VT_TELNET_WONT = 252, // FC
  VT_TELNET_DO = 253, // FD
  VT_TELNET_DONT = 254, // FE
  VT_TELNET_IAC = 255, // FF
};

void vt_init(vt_t *vt, int (*exec)(char *), vt_fp fp, int sock, const char *promt);
void vt_input(vt_t *vt, uint8_t rx);

#endif /* APPS_VT_H_ */
