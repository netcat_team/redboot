/*
 * syslog.h
 *
 *  Created on: Nov 2, 2018
 *      Author: netcat
 */

#ifndef SYSLOG_H_
#define SYSLOG_H_

#include <stdint.h>

#define MAX_SYSLOG_MESSAGE 128

#define ESC_CLR_BLACK        "\x1B[0;30m"
#define ESC_CLR_RED          "\x1B[0;31m"
#define ESC_CLR_GREEN        "\x1B[0;32m"
#define ESC_CLR_YELLOW       "\x1B[0;33m"
#define ESC_CLR_BLUE         "\x1B[0;34m"
#define ESC_CLR_MAGENTA      "\x1b[0;35m"
#define ESC_CLR_CYAN         "\x1b[0;36m"
#define ESC_CLR_WHITE        "\x1b[0;37m"

#define ESC_BCLR_BLACK       "\x1B[1;30m"
#define ESC_BCLR_RED         "\x1B[1;31m"
#define ESC_BCLR_GREEN       "\x1B[1;32m"
#define ESC_BCLR_YELLOW      "\x1B[1;33m"
#define ESC_BCLR_BLUE        "\x1B[1;34m"
#define ESC_BCLR_MAGENTA     "\x1b[1;35m"
#define ESC_BCLR_CYAN        "\x1b[1;36m"
#define ESC_BCLR_WHITE       "\x1b[1;37m"

#define ESC_CLR_RESET        "\x1B[0m"

enum
{
  SYSLOG_EMERG,
  SYSLOG_ALERT,
  SYSLOG_CRIT,
  SYSLOG_ERR,
  SYSLOG_WARNING,
  SYSLOG_NOTICE,
  SYSLOG_INFO,
  SYSLOG_DEBUG,
};

void syslog(uint32_t pri, const char *fmt, ...);
void slog(const char * fmt, ...);

#endif /* SYSLOG_H_ */
