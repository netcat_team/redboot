/*
 * heap.h
 *
 *  Created on: Jul 22, 2019
 *      Author: netcat
 */

#ifndef SYS_HEAP_H_
#define SYS_HEAP_H_

#include <stdint.h>

typedef struct heap_block_self
{
  uint32_t size;
  struct heap_block_self *next;
} heap_block_t;

typedef struct
{
  uint32_t addr;
  uint32_t size;
  uint32_t remaining;
  heap_block_t *first;
  heap_block_t *last;
} heap_header_t;

#define HEAP_ALIGN_ADDR(x) (((x) & (sizeof(int) - 1)) ? ((sizeof(int) - ((x) & (sizeof(int) - 1)))) : 0)

void heap_conf(heap_header_t *heap, uint32_t addr, uint32_t size);
void *heap_alloc(heap_header_t *heap, uint32_t size);
void heap_free(heap_header_t *heap, void *ptr);

#endif /* SYS_HEAP_H_ */
