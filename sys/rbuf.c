/*
 * heap.c
 *
 *  Created on: Jul 22, 2019
 *      Author: netcat
 */

#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include "rbuf.h"
#include "heap.h"

void rbuf_init(rbuf_t *rb, int size)
{
  rb->size = size + 1;
  rb->data = malloc(size + 1);
  rb->head = 0;
  rb->tail = 0;
}

void rbuf_destroy(rbuf_t *rb)
{
  free(rb->data);
}

void rbuf_clear(rbuf_t *rb)
{
  rb->head = 0;
  rb->tail = 0;
}

int rbuf_count(const rbuf_t *rb)
{
  return (rb->tail >= rb->head) ? (rb->tail - rb->head) : (rb->size - rb->head + rb->tail);
}

int rbuf_capacity(const rbuf_t *rb)
{
  return rb->size - 1;
}

int rbuf_put(rbuf_t *rb, uint8_t s)
{
  if(rbuf_count(rb) >= rbuf_capacity(rb)) return 0;

  rb->data[rb->tail] = s;
  if(++rb->tail >= rb->size) rb->tail = 0;
  return 1;
}

int rbuf_get(rbuf_t *rb, uint8_t *s)
{
  if(!rbuf_count(rb)) return 0;

  *s = rb->data[rb->head];
  if(++rb->head >= rb->size) rb->head = 0;
  return 1;
}
