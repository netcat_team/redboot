/*
 * ota_ctl.h
 *
 *  Created on: May 19, 2020
 *      Author: netcat
 */

#ifndef OTA_CTL_H_
#define OTA_CTL_H_

#include <stdint.h>

#define PKG_CRC_SIZE 1
#define OTA_BUF_SIZE (1024 * 4 + 32)

typedef struct __attribute__((packed))
{
  uint8_t flags;
  uint8_t seq;
  uint16_t cmd;
  uint8_t payload[0];
} pkg_header_t;

enum
{
  CMD_ACC_PING,
  CMD_ACC_MEM,
  CMD_ACC_BOOT,
  CMD_ACC_REBOOT,
  CMD_ACC_WAKEUP,
};

void ota_event(uint16_t cmd);
void ota_rcv(uint8_t rx);

#endif /* OTA_CTL_H_ */
