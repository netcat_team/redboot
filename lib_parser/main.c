#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <sys/types.h>
#include <stdlib.h>

typedef struct __attribute__ ((packed))
{
  uint8_t mag[4];
  uint8_t class;
  uint8_t data;
  uint8_t version;
  uint8_t os_abi;
  uint8_t abi_version;
  uint8_t padding[7];
} elf_ident_t;

typedef struct __attribute__ ((packed))
{
  elf_ident_t ident;
  uint16_t type;
  uint16_t machine;
  uint32_t version;
  uint32_t entry;
  uint32_t phoff;
  uint32_t shoff;
  uint32_t flags;
  uint16_t ehsize;
  uint16_t phentsize;
  uint16_t phnum;
  uint16_t shentsize;
  uint16_t shnum;
  uint16_t shstrndx;
} elf_header_t;

typedef struct __attribute__ ((packed))
{
  uint32_t type;
  uint32_t offset;
  uint32_t vaddr;
  uint32_t paddr;
  uint32_t filesz;
  uint32_t memsz;
  uint32_t flags;
  uint32_t align;
} elf_p_header_t;

typedef struct __attribute__ ((packed))
{
  uint32_t name;
  uint32_t type;
  uint32_t flags;
  uint32_t addr;
  uint32_t offset;
  uint32_t size;
  uint32_t link;
  uint32_t info;
  uint32_t addralign;
  uint32_t entsize;
} elf_s_header_t;

typedef struct
{
  uint32_t name;
  uint32_t value;
  uint32_t size;
  uint8_t info;
  uint8_t other;
  uint16_t shndx;
} elf_symtab_header_t;

int main(int argc, char **argv)
{
  size_t res = 0;

  if(argc <= 1)
  {
    printf("please choose a file \n");
    return;
  }

  FILE *f = fopen(argv[1], "r+b");
  if(f)
  {
    elf_header_t header;

    fseek(f, 0, SEEK_SET);
    res = fread(&header, 1, sizeof(elf_header_t), f);

    // printf("magic:   \t%6c%c%c \n", header.ident.mag[1], header.ident.mag[2], header.ident.mag[3]);
    // printf("class:   \t%8x \n", header.ident.class);
    // printf("data:    \t%8x \n", header.ident.data);
    // printf("ver:     \t%8x \n", header.ident.version);
    // printf("os_abi:  \t%8x \n", header.ident.os_abi);
    // printf("abi_ver: \t%8x \n", header.ident.abi_version);
    // printf("\n");

    // printf("type:      \t%8x \n", header.type);
    // printf("machine:   \t%8x \n", header.machine);
    // printf("version:   \t%8x \n", header.version);
    // printf("entry:     \t%8x \n", header.entry);
    // printf("phoff:     \t%8x \n", header.phoff);
    // printf("shoff:     \t%8x \n", header.shoff);
    // printf("flags:     \t%8x \n", header.flags);
    // printf("ehsize:    \t%8x \n", header.ehsize);
    // printf("phentsize: \t%8x \n", header.phentsize);
    // printf("phnum:     \t%8d \n", header.phnum);
    // printf("shentsize: \t%8x \n", header.shentsize);
    // printf("shnum:     \t%8d \n", header.shnum);
    printf("shstrndx:  \t%8d \n", header.shstrndx);

    printf("\n---- program ----\n");

    printf("Type   Offset   V Addr   P Addr   FileSize  MemSize    Align Flg \n");

    if(header.phoff)
    {
      fseek(f, header.phoff, SEEK_SET);

      int i;
      elf_p_header_t p_header;
      for(i = 0; i < header.phnum; i++)
      {
        res = fread(&p_header, 1, sizeof(elf_p_header_t), f);

        // printf("\n");
        // printf("type:   \t%8x \n", p_header.type);
        // printf("offset: \t%8x \n", p_header.offset);
        // printf("vaddr:  \t%8x \n", p_header.vaddr);
        // printf("paddr:  \t%8x \n", p_header.paddr);
        // printf("filesz: \t%8x \n", p_header.filesz);
        // printf("memsz:  \t%8x \n", p_header.memsz);
        // printf("flags:  \t%8x \n", p_header.flags);
        // printf("align:  \t%8x \n", p_header.align);

        printf("%4x %8x %8x %8x   %8x %8x %8x %4x \n", p_header.type, p_header.offset, p_header.vaddr, p_header.paddr,
          p_header.filesz, p_header.memsz, p_header.align, p_header.flags);

      }
    }

    int dynsym_table = 0;
    int dynsym_table_size = 0;
    char *string_array = 0;

    int dynsym_str = 0;
    int dynsym_str_size = 0;
    char *string_array2 = 0;

    if(header.phoff)
    {
      printf("\n--- sections ----\n");
      printf("Num:     Addr  Offset    Size   (dec)  Type  Flag Name\n");

      elf_s_header_t s_header;

      // read str table
      fseek(f, header.shoff + sizeof(elf_s_header_t) * header.shstrndx, SEEK_SET);
      res = fread(&s_header, 1, sizeof(elf_s_header_t), f);
      string_array = malloc(s_header.size);
      fseek(f, s_header.offset, SEEK_SET);
      res = fread(string_array, 1, s_header.size, f);

      fseek(f, header.shoff, SEEK_SET);

      int i;
      
      for(i = 0; i < header.shnum; i++)
      {
        res = fread(&s_header, 1, sizeof(elf_s_header_t), f);
        // if(!s_header.addr) continue;

        s_header.type &= 0xff;

        printf(" %2d: %8x %7x %7x %7d  %c%c%c%c  %c%c%c %s \n", i, s_header.addr, s_header.offset,
          s_header.size, s_header.size,
          s_header.type == 1 ? 'P' : ' ',
          s_header.type == 2 ? 'T' : ' ',
          s_header.type == 3 ? 'S' : ' ',
          s_header.type == 8 ? 'N' : ' ',
          s_header.flags & 0x4 ? 'E' : ' ',
          s_header.flags & 0x2 ? 'A' : ' ',
          s_header.flags & 0x1 ? 'W' : ' ',
          &string_array[s_header.name] );
      
        if(!strcmp(&string_array[s_header.name], ".dynsym"))
        {
          dynsym_table = s_header.offset;
          dynsym_table_size = s_header.size / sizeof(elf_symtab_header_t);
        }
        if(!strcmp(&string_array[s_header.name], ".dynstr"))
        {
          dynsym_str = s_header.offset;
          dynsym_str_size = s_header.size;
        }
      }
    }

    if(dynsym_str)
    {
      fseek(f, dynsym_str, SEEK_SET);
      string_array2 = malloc(dynsym_str_size);
      res = fread(string_array2, 1, dynsym_str_size, f);

      printf("\n--- dynsym ----\n");
      printf("Num:   Value  Size Type Other Ndx   Name\n");

      fseek(f, dynsym_table, SEEK_SET);

      int i;
      for(i = 0; i < dynsym_table_size; i++)
      {
        elf_symtab_header_t symtab_header;
        res = fread(&symtab_header, 1, sizeof(elf_symtab_header_t), f);
        
        printf("%2d: %8x %5x %4x %5x %3x   %s \n", i, symtab_header.value, symtab_header.size, symtab_header.info, symtab_header.other,
          symtab_header.shndx, &string_array2[symtab_header.name]);
      }
    }

    fclose(f);
  }
  else
  {
    res++;
    printf("no file\n");
  }

  return 0;
}
