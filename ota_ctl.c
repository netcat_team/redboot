/*
 * ota_ctl.c
 *
 *  Created on: May 19, 2020
 *      Author: netcat
 */

#include "bsp/bsp.h"
#include "hal/hw_gpio.h"
#include "hal/hw_uart.h"
#include "sys/rbuf.h"
#include "hw_crc.h"
#include "ota_ctl.h"
#include "printf.h"
#include "memmap.h"

#define BS1 0x7e
#define BS2 0x7d
#define BS3 0x5e
#define BS4 0x5d

static void stuff_move(uint8_t *buf, uint32_t size)
{
  for(uint32_t i = 0; i < size; i++) buf[i] = buf[i + 1];
}

static uint32_t stuff_cnt(const iovec *vec, uint32_t vec_len)
{
    uint32_t cnt = 0;
    for(int i = 0; i < vec_len; i++)
    {
        for(int j = 0; j < vec[i].len; j++)
        {
            uint8_t t = ((uint8_t *)vec[i].data)[j];
            if(t == BS1 || t == BS2) cnt++;
            cnt++;
        }
    }
    return cnt;
}

void stuff(iovec *ret, iovec *vec, uint32_t vec_len)
{
  uint8_t *out = ret->data;

  *out++ = BS1;

  for(int i = 0; i < vec_len; i++)
  {
    for(int j = 0; j < vec[i].len; j++)
    {
      uint8_t t = ((uint8_t *)vec[i].data)[j];

      if(t == BS1)
      {
        *out++ = BS2;
        *out++ = BS3;
      }
      else if(t == BS2)
      {
        *out++ = BS2;
        *out++ = BS4;
      }
      else *out++ = t;
    }
  }

  *out++ = BS1;
}

void vec_malloc_stuff(iovec *ret, iovec *vec, uint32_t vec_len)
{
  uint32_t size_stuff = stuff_cnt(vec, vec_len) + 2;
  ret->len = size_stuff;
  ret->data = malloc(size_stuff);
  stuff(ret, vec, vec_len);
}

void *unstuff(iovec *vec)
{
  uint32_t size = vec->len;
  uint8_t *buf = vec->data;

  for(uint32_t i = 0; i < size; i++)
  {
    uint8_t t = buf[i];

    if(t == BS1)
    {
      size--;
      stuff_move(&buf[i], size - i);
    }
    else if(t == BS2)
    {
      size--;
      stuff_move(&buf[i], size - i);

      t = buf[i];
      if(t == BS3) buf[i] = BS1;
      else if(t == BS4) buf[i] = BS2;
    }
  }

  vec->len = size;
  return vec->data;
}

static void ota_reply(pkg_header_t *pkg, iovec *pl)
{
  uint8_t crc = 0;
  iovec vec[] = {
     { pkg, sizeof(pkg_header_t) },
     { pl ? pl->data : 0, pl ? pl->len : 0 },
     { &crc, sizeof(crc) }};
  crc = crc_calc8(vec, SIZE_OF_ARRAY(vec) - 1);

  iovec v;
  vec_malloc_stuff(&v, vec, SIZE_OF_ARRAY(vec));
  uart_io(USART3, &v, 1, 0, 0);
}

void ota_event(uint16_t cmd)
{
  pkg_header_t h = { 0, 0, cmd };
  ota_reply(&h, 0);
}

void ota_pkg(iovec *msg)
{
  unstuff(msg);
//  uint8_t *t = msg->data;

  // crc check
  iovec vec = { msg->data, msg->len - PKG_CRC_SIZE };
  uint8_t crc = crc_calc8(&vec, 1);
  if(crc != ((uint8_t *)msg->data)[msg->len - PKG_CRC_SIZE])
  {
    printf("bad crc \n");
    return;
  }

  pkg_header_t *pkg = msg->data;
  iovec pl = { pkg->payload, msg->len - (sizeof(pkg_header_t) + PKG_CRC_SIZE) };

  switch(pkg->cmd)
  {
    case CMD_ACC_PING:
      break;

    case CMD_ACC_REBOOT:
      ota_reply(pkg, &pl);
      break;

    case CMD_ACC_MEM:
    {
      struct __attribute__((packed))
      {
        uint32_t addr;
        uint8_t buf[];
      } *msg = pl.data;

//      printf("w: %04x [%d]\n", msg->addr, pl.len);
      memcpy((void *)msg->addr, msg->buf, pl.len - sizeof(uint32_t));
      ota_reply(pkg, 0);
    }
    break;

    case CMD_ACC_BOOT:
    {
      uint32_t addr = *(uint32_t *)pl.data;
      ota_reply(pkg, 0);

//      printf("Starting firmware... 0x%x \n", addr);
      SCB->VTOR = addr;
      uint32_t *p = (uint32_t *)addr;
      __set_MSP(*p);
      p++;
//      void (*pMain)(void) = (void (*)(void))(*p);
//      pMain();
      ((void(*)(void))*p)();
    }
    break;

  default:
    break;
  }
}

void ota_rcv(uint8_t rx)
{
  static uint8_t buf[OTA_BUF_SIZE];
  static uint32_t len = 0;

  // ctrl symbol ?
  if(rx == BS1)
  {
    if(len < 2)
    {
      len = 0;
      buf[len++] = rx;
    }
    else
    {
      // the correct pkg start with ctrl symbol
      if(buf[0] == BS1)
      {
        buf[len++] = rx;
        if(len > OTA_BUF_SIZE) len = OTA_BUF_SIZE;

        iovec pkg = { buf, len };
        ota_pkg(&pkg);
      }
      len = 0;
    }
  }
  // data symbol
  else
  {
    buf[len++] = rx;
    if(len > OTA_BUF_SIZE) len = OTA_BUF_SIZE;
  }
}


