NAME = redboot
ODIR = obj
MCU = cortex-m4
TOOLCHAIN = arm-none-eabi-

CC = $(TOOLCHAIN)gcc
CPPC = $(TOOLCHAIN)g++
OBJCOPY = $(TOOLCHAIN)objcopy
GDB = $(TOOLCHAIN)gdb
SIZE = $(TOOLCHAIN)size

# openocd config
DBG = stlink-v2
DBG_IF = hla_swd
DBG_MCU = stm32f4x

# ld
LDFILE = bsp/hal/f429/link.ld

# src
SRC = main.c
SRC += ota_ctl.c

# sys
SRC += sys/cli/printf.c
SRC += sys/cli/getopt.c

SRC += sys/cli/syslog.c
SRC += sys/cli/vt.c
SRC += sys/cli/apps.c
SRC += sys/heap.c
SRC += sys/rbuf.c

# hal
SRC += bsp/bsp.c
SRC += bsp/hal/hw_fmc.c
SRC += bsp/hal/hw_gpio.c
SRC += bsp/hal/hw_rcc.c
SRC += bsp/hal/hw_uart.c
SRC += bsp/hal/hw_ltdc.c
SRC += bsp/hal/hw_dma2d.c
SRC += bsp/hal/hw_i2c.c
SRC += bsp/hal/hw_sdio.c
SRC += bsp/hal/hw_spi.c
SRC += bsp/hal/hw_crc.c
SRC += bsp/hal/hw_rtc.c
SRC += bsp/hal/hw_tim.c
SRC += bsp/hal/hw_rng.c
SRC += bsp/hal/hw_adc.c
SRC += bsp/hal/f429/startup.c
SRC += bsp/tps65217.c
SRC += bsp/w25q64.c
SRC += bsp/rm68120.c
SRC += bsp/gma.c
SRC += bsp/s2303a.c

# fatfs
SRC += fs/fatfs.c
SRC += fs/mbr.c
SRC += fs/vfs.c

#apps
SRC += $(wildcard apps/*.c)

OBJS = $(patsubst %,$(ODIR)/%, ${SRC:.c=.o})
CPPOBJS = $(patsubst %,$(ODIR)/%, ${CPPSRC:.cpp=.o})

#inc
INC += .
INC += bsp
INC += bsp/hal
INC += bsp/hal/f429
INC += bsp/hal/cmsis
INC += fs
INC += sys
INC += sys/cli
_INC = $(INC:%=-I%)

CFLAGS = -std=gnu99 -mcpu=$(MCU) -mthumb -g -Wall -O1 -mfloat-abi=softfp -mfpu=fpv4-sp-d16 -ffunction-sections -fdata-sections
CFLAGS += -Wno-implicit-function-declaration
CPPFLAGS = $(CFLAGS) -fno-rtti -fno-exceptions -std=c++11 -Wno-implicit-function-declaration
LDFLAGS = -mcpu=$(MCU) -mthumb -T${LDFILE} -flto
LDFLAGS += -Wl,--gc-sections
LDFLAGS += -specs=nano.specs -specs=nosys.specs -nostartfiles #-nostdlib

.PHONY : all
all: $(NAME).elf $(NAME).bin size

$(ODIR)/%.o: %.c
	@mkdir -p $(ODIR)/$(dir $<)
	@echo "  CC\t$<"
	@$(CC) $(CFLAGS) $(_INC) -c $< -o $@

#$(ODIR)/%.o: %.cpp
#	@mkdir -p $(ODIR)/$(dir $<)
#	@echo "  CC\t$<"
#	@$(CPPC) $(CPPFLAGS) $(_INC) -c $< -o $@

$(NAME).elf: $(OBJS) $(CPPOBJS)
	@echo "  LD\t$@"
	@$(CPPC) $^ -o $@ $(LDFLAGS)

$(NAME).bin: $(NAME).elf
	@echo "  OC\t$@"
	@$(OBJCOPY) -O binary -S $^ $@

size: $(NAME).elf
	@$(SIZE) $(NAME).elf

flash: $(NAME).elf
	@openocd -f interface/$(DBG).cfg \
		-c"transport select $(DBG_IF)" \
		-f target/$(DBG_MCU).cfg \
		-c"init" \
		-c"reset halt" \
		-c"flash write_image erase $(NAME).elf" \
		-c"reset run" \
		-c"shutdown"

debug: $(NAME).elf
	@openocd -f interface/$(DBG).cfg \
		-c"transport select $(DBG_IF)" \
		-f target/$(DBG_MCU).cfg \
		-c"init" \
		-c"reset halt" \
		-c"flash write_image erase $(NAME).elf" \
		-c"reset halt"

rst: $(NAME).elf
	@openocd -f interface/$(DBG).cfg \
		-c"transport select $(DBG_IF)" \
		-f target/$(DBG_MCU).cfg \
		-c"init" \
		-c"reset run" \
		-c"shutdown"

gdb:
	$(GDB) --eval-command="target remote :3333" -tui $(NAME).elf

clean:
	@rm -f $(OBJS)
	@rm -f $(CPPOBJS)
	@rm -rf $(ODIR)
	@rm -f $(NAME).elf $(NAME).bin $(NAME).map

