/*
 * gma.c
 *
 *  Created on: Jan 4, 2016
 *      Author: netcat
 */

#include "rm68120.h"
#include "hw_dma2d.h"
#include "memmap.h"
#include "gma.h"

uint32_t vbuf = LCD_FB_ADDR;
uint32_t vbuf_clut = LCD_CLUT_ADDR;

void gma_set_color(uint32_t clr)
{
  dma2d_set_clut_color(clr);
}

void gma_draw_hline_clut(uint32_t x, uint32_t y, uint32_t w)
{
  uint32_t offset = LCD_BPP * (LCD_MAXX * y + x);

  dma2d_rst();
  dma2d_mode(DMA2D_M2M_BLEND);
  dma2d_cfg(0, 1, w, vbuf + offset);
  dma2d_fg_bg_cfg_clut(vbuf_clut, vbuf + offset, 0, 0);

  dma2d_start();
  dma2d_wait();
}

void gma_draw_vline_clut(uint32_t x, uint32_t y, uint32_t h)
{
  uint32_t offset = LCD_BPP * (LCD_MAXX * y + x);

  dma2d_rst();
  dma2d_mode(DMA2D_M2M_BLEND);
  dma2d_cfg(LCD_MAXX - 1, h, 1, vbuf + offset);
  dma2d_fg_bg_cfg_clut(vbuf_clut, vbuf + offset, LCD_MAXX - 1, 0);

  dma2d_start();
  dma2d_wait();
}

void gma_fill_rect_dma_clut(uint32_t x, uint32_t y, uint32_t w, uint32_t h)
{
  uint32_t offset = LCD_BPP * (LCD_MAXX * y + x);

  dma2d_rst();
  dma2d_mode(DMA2D_M2M_BLEND);
  dma2d_cfg(LCD_MAXX - w, h, w, vbuf + offset);
  dma2d_fg_bg_cfg_clut(vbuf_clut, vbuf + offset, LCD_MAXX - w, 0);

  dma2d_start();
  dma2d_wait();
}

void gma_fast_fill_rect_dma(uint32_t vbuf_to, uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint32_t c)
{
  uint32_t offset = LCD_BPP * (LCD_WIDTH * y + x);

  dma2d_rst();
  dma2d_mode(DMA2D_R2M);
  dma2d_set_clr(c);
  dma2d_cfg(LCD_WIDTH - w, h, w, vbuf_to + offset);
  dma2d_start();
  dma2d_wait();
}

void gma_copy_layer_rect(uint32_t vbuf_to, uint32_t vbuf_from, uint32_t x, uint32_t y, uint32_t w, uint32_t h)
{
  dma2d_rst();
  dma2d_mode(DMA2D_M2M);
  dma2d_cfg(LCD_WIDTH - w, h, w, vbuf_to);
  dma2d_fg_bg_cfg_cp(vbuf_from, vbuf_to, LCD_WIDTH - w);

  dma2d_start();
  dma2d_wait();
}

void gma_point(uint32_t x, uint32_t y)
{
//  if(x < vbuf_minx || x >= vbuf_maxx || y < vbuf_miny || y >= vbuf_maxy) return;

  uint32_t offset = LCD_BPP * (LCD_WIDTH * y + x);

  dma2d_rst();
  dma2d_mode(DMA2D_M2M_BLEND);
  dma2d_cfg(0, 1, 1, vbuf + offset);
  dma2d_fg_bg_cfg_clut(vbuf_clut, vbuf + offset, 0, 0);

  dma2d_start();
  dma2d_wait();
}

void gma_img32(const uint8_t *data, int32_t x, int32_t y, int32_t w, int32_t h)
{
  dma2d_rst();
  dma2d_mode(DMA2D_M2M);
  dma2d_cfg(LCD_WIDTH - w, h, w, vbuf);
  dma2d_fg_bg_cfg_cp((int)data, vbuf, LCD_WIDTH - w);

  dma2d_start();
  dma2d_wait();
}

void gma_img8(const uint8_t *data, int32_t x, int32_t y, int32_t w, int32_t h, uint32_t clr)
{
  uint32_t offset = LCD_BPP * (LCD_WIDTH * y + x);

  dma2d_rst();
  dma2d_mode(DMA2D_M2M_BLEND);
  DMA2D->FGCOLR = clr;
  DMA2D->BGCOLR = clr;
  dma2d_set_clr(clr);
  dma2d_cfg(LCD_WIDTH - w, h, w, vbuf + offset);
  dma2d_fg_bg_cfg_clut((uint32_t)data, vbuf + offset, LCD_MAXX - w, 1);

  dma2d_start();
  dma2d_wait();
}
