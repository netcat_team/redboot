/*
 * w25q64.h
 *
 *  Created on: Apr 6, 2019
 *      Author: vchumakov
 */

#ifndef BSP_W25Q64_H_
#define BSP_W25Q64_H_

#include "hw_defs.h"

#define W25Q64_BLOCK_SIZE 0x10000
#define W25Q64_SECTOR_SIZE 0x1000
#define W25Q64_PAGE_SIZE 0x100

#define W25Q64_PAGE_ERASE_MS 400
#define W25Q64_PAGE_WRITE_MS 3

enum
{
  SPIMEM_CMD_WRITE = 0x02,
  SPIMEM_CMD_READ = 0x03,
  SPIMEM_CMD_WE_DIS = 0x04,
  SPIMEM_CMD_READ_ST = 0x05,
  SPIMEM_CMD_WE_EN = 0x06,
  SPIMEM_CMD_ERASE_SECTOR = 0x20,
  SPIMEM_CMD_ERASE_BLOCK = 0x52,
  SPIMEM_CMD_ERASE_CHIP = 0x60,
  SPIMEM_CMD_READ_ID = 0x9F,
};

#define SPIMEM_SR_WEL 1
#define SPIMEM_SR_RDY 0

uint8_t spimem_id();
uint32_t spimem_write(uint32_t page, uint8_t *data, uint32_t count);
uint32_t spimem_read(uint32_t addr, uint8_t *buf, uint32_t size);
void spimem_erase_sector(uint32_t addr);

#endif /* BSP_W25Q64_H_ */
