/*
 * rm68120.c
 *
 *  Created on: Apr 9, 2017
 *      Author: netcat
 */

#include "stm32f429xx.h"
#include "rm68120.h"
#include "bsp.h"
#include "hal/hw_defs.h"
#include "hal/hw_gpio.h"
#include "hal/hw_spi.h"
#include "sys/cli/syslog.h"

void lcd_cs(uint32_t select)
{
  if(select) gpio_set(DGB_MCU_TEST_PORT, DGB_MCU_TEST_PIN);
  else gpio_reset(DGB_MCU_TEST_PORT, DGB_MCU_TEST_PIN);
}

static spi_dev lcd_spi = { SPI1, lcd_cs, 0 };

typedef struct
{
  uint16_t reg;
  uint8_t val;
  uint8_t wait_10ms;
} lcd_conf_t;

const lcd_conf_t rm68120_conf[] = {
    { 0xF000, 0x55 }, // Enable Page 1
    { 0xF001, 0xAA },
    { 0xF002, 0x52 },
    { 0xF003, 0x08 },
    { 0xF004, 0x01 },

    { 0xB600, 0x34 }, // AVDD Setting
    { 0xB601, 0x34 },
    { 0xB602, 0x34 },
    { 0xB000, 0x0c },
    { 0xB001, 0x0c },
    { 0xB002, 0x0c },

    { 0xB700, 0x34 }, // AVEE Setting
    { 0xB701, 0x34 },
    { 0xB702, 0x34 },
    { 0xB100, 0x0d },
    { 0xB101, 0x0d },
    { 0xB102, 0x0d },

    { 0xB800, 0x24 }, // VCL Setting
    { 0xB200, 0x00 },

    { 0xBf00, 0x01 },

    { 0xB900, 0x24 }, // VGH Setting
    { 0xB901, 0x24 },
    { 0xB902, 0x24 },
    { 0xB300, 0x05 },
    { 0xB301, 0x05 },
    { 0xB302, 0x05 },

    { 0xBA00, 0x34 }, // VGL Setting
    { 0xBA01, 0x34 },
    { 0xBA02, 0x34 },
    { 0xB500, 0x0b },
    { 0xB501, 0x0b },
    { 0xB502, 0x0b },

    { 0xBC00, 0x00 }, // VGMP Setting
    { 0xBC01, 0x80 },
    { 0xBC02, 0x00 },

    { 0xBD00, 0x00 }, // VGMN Setting
    { 0xBD01, 0x80 },
    { 0xBD02, 0x00 },

    { 0xBE00, 0x00 }, // Vcom Setting
    { 0xBE01, 0x59 }, // 2

    { 0xB400, 0x10 },

    { 0xB000, 0x08 }, // RGB I/F Setting
    { 0xB001, 0x05 },
    { 0xB002, 0x02 },
    { 0xB003, 0x05 },
    { 0xB004, 0x02 },

    { 0xB600, 0x05 }, // SDT

    { 0xFF00, 0xAA },
    { 0xFF01, 0x55 },
    { 0xFF02, 0x25 },
    { 0xFF03, 0x01 },

    { 0xF304, 0x11 },
    { 0xF306, 0x10 },
    { 0xF408, 0x00 },

    { 0x3500, 0x00 },
    { 0x3600, 0xD4 }, // rotate
    { 0x3A00, 0x55 },
    { 0x3B00, 0x28 },

    { 0x1100, 0x00, 1 }, // sleep out
    { 0x2900, 0x00, 1 }, // display on
    { 0x2c00, 0x00 },
};

const lcd_conf_t rm68120_set_addr0[] = {
    { 0x2a00, 0x00 },
    { 0x2a01, 0x00 },
    { 0x2a02, 0x00 },
    { 0x2a03, 0x00 },

    { 0x2b00, 0x00 },
    { 0x2b01, 0x00 },
    { 0x2b02, 0x00 },
    { 0x2b03, 0x00 },

    { 0x2c00, 0x00 },
};

static void rm68120_apply_conf(const lcd_conf_t *conf, uint32_t size)
{
  const lcd_conf_t *end = conf + size;
  while(conf++ < end)
  {
#if 0
    LCD_WRITE_REG(reg);
    LCD_WRITE_DATA(val);
#else
    uint8_t cmd[] = { 0x20, conf->reg >> 8, 0x00, conf->reg, 0x04, conf->val };
    const iovec v[] = { { &cmd, sizeof(cmd) } };
    spi_io(&lcd_spi, v, SIZE_OF_ARRAY(v), 0, 0);
#endif

//    syslog(SYSLOG_DEBUG, "LCD[0x%04x] = 0x%02x", conf->reg, conf->val);

    if(conf->wait_10ms)
    {
      wait_ms(conf->wait_10ms);
//      syslog(SYSLOG_DEBUG, "LCD wait %d ms", conf->wait_10ms * 10);
    }
  }
}

void lcd_set_addr0()
{
  rm68120_apply_conf(rm68120_set_addr0, SIZE_OF_ARRAY(rm68120_set_addr0));
}

void lcd_init()
{
  rm68120_apply_conf(rm68120_conf, SIZE_OF_ARRAY(rm68120_conf));
}
