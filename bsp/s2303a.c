/*
 * s2303a.c
 *
 *  Created on: Feb 1, 2016
 *      Author: netcat
 */

#include "hw_i2c.h"
#include "s2303a.h"

uint32_t s3203_read(uint8_t reg, void *buf, uint32_t cnt)
{
  uint8_t dev = S2303A_ADDR;
  const iovec v[] = { { &dev, sizeof(dev) }, { &reg, sizeof(reg) } };
  return i2c_io(I2C3, v, SIZE_OF_ARRAY(v), buf, cnt);
}

ts_t s3203_ts()
{
  uint8_t st;
  ts_pos_t pos;
  s3203_read(S2303A_REG_ST, &st, sizeof(st));
  s3203_read(S2303A_REG_POS, &pos, sizeof(pos));
  ts_t ret = { pos.x1 * 7, pos.y1 * 7, st & 0x01 };
  return ret;
}
