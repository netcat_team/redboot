/*
 * gma.h
 *
 *  Created on: Apr 29, 2020
 *      Author: netcat
 */

#ifndef BSP_GMA_H_
#define BSP_GMA_H_

#include "hw_defs.h"
#include "memmap.h"

#define RGBA(r, g, b, a) ((uint32_t)(((a) << 24) | ((r) << 16) | ((g) << 8) | (b)))
#define RGB(r, g, b) RGBA(r, g, b, 255)

void gma_set_color(uint32_t clr);
void gma_draw_hline_clut(uint32_t x, uint32_t y, uint32_t w);
void gma_draw_vline_clut(uint32_t x, uint32_t y, uint32_t h);
void gma_fill_rect_dma_clut(uint32_t x, uint32_t y, uint32_t w, uint32_t h);
void gma_fast_fill_rect_dma(uint32_t vbuf_to, uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint32_t c);
void gma_point(uint32_t x, uint32_t y);
void gma_img32(const uint8_t *data, int32_t x, int32_t y, int32_t w, int32_t h);
void gma_img8(const uint8_t *data, int32_t x, int32_t y, int32_t w, int32_t h, uint32_t clr);

#endif /* BSP_GMA_H_ */
