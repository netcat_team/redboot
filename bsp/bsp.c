/*
 * bsp.c
 *
 *  Created on: Jul 23, 2019
 *      Author: netcat
 */

#include "hw_gpio.h"
#include "hw_rcc.h"
#include "hw_fmc.h"
#include "hw_ltdc.h"
#include "hw_uart.h"
#include "hw_i2c.h"
#include "hw_sdio.h"
#include "hw_spi.h"
#include "hw_dma2d.h"
#include "hw_rng.h"
#include "hw_rtc.h"
#include "hw_adc.h"
#include "memmap.h"

#include "sys/cli/syslog.h"
#include "sys/cli/printf.h"
#include "sys/cli/vt.h"
#include "sys/cli/apps.h"

#include "bsp/tps65217.h"
#include "bsp/w25q64.h"
#include "bsp/rm68120.h"
#include "bsp/bsp.h"
#include "bsp/bsp.h"
#include "vfs.h"

#include "heap.h"

heap_header_t sys_heap;

void *malloc(size_t size)
{
  void *block = heap_alloc(&sys_heap, size);
//  syslog(SYSLOG_DEBUG, "malloc: 0x%08x, size: %d", (uint32_t)((uint32_t *)block), size);
  return block;
}

void free(void *block)
{
//  syslog(SYSLOG_DEBUG, "free: 0x%08x", (uint32_t)((uint32_t *)block));
  heap_free(&sys_heap, block);
}

void bsp_cli(char c)
{
  uart_out(USART1, c);
  if(c == '\n') uart_out(USART1, '\r');
}

void bsp_cli_str(const char *s)
{
  while(*s) bsp_cli(*s++);
}

void bsp_ld_print()
{
  extern unsigned int _stext, _etext, _sdata, _edata, _sbss, _ebss;
  printf("text: %d, data: %d, bss: %d \n", (int)&_etext - (int)&_stext,
    (int)&_edata - (int)&_sdata, (int)&_ebss - (int)&_sbss);
}

void bsp_io_init()
{
  // PWR_EN
//  gpio_init(PWR_ENABLE_PORT, PWR_ENABLE_PIN, GPIO_MODE_OUT);
//  gpio_set(PWR_ENABLE_PORT, PWR_ENABLE_PIN);

  // ESP_CE
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIOHEN;
  gpio_init(ESP_CE_PORT, ESP_CE_PIN, GPIO_MODE_OUT);
  gpio_reset(ESP_CE_PORT, ESP_CE_PIN); // disable ESP

  // DGB_MCU_TEST - lcd CS
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIOJEN;
  gpio_init(DGB_MCU_TEST_PORT, DGB_MCU_TEST_PIN, GPIO_MODE_OUT);
  gpio_set(DGB_MCU_TEST_PORT, DGB_MCU_TEST_PIN);

  // leds
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIOHEN;
  gpio_init(LEDS_PORT, LED_GREEN_PIN, GPIO_MODE_OUT);
  gpio_init(LEDS_PORT, LED_YELLOW_PIN, GPIO_MODE_OUT);

  // SDHC_ST
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIOKEN;
  gpio_init_ext(SDHC_ST_PORT, SDHC_ST_PIN, GPIO_MODE_IN, GPIO_OTYPE_PP, GPIO_SPEED_FREQ_LOW, GPIO_PULLUP);

  // EPCS_CS (FPGA NAND)
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIOJEN;
  gpio_init(EPCS_CS_PORT, EPCS_CS_PIN, GPIO_MODE_OUT);
  gpio_set(EPCS_CS_PORT, EPCS_CS_PIN);

  // EPCS_CS (FPGA)
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;
  gpio_init(FPGA_CONF_DONE_PORT, FPGA_CONF_DONE_PIN, GPIO_MODE_IN);
  gpio_init_ext(FPGA_CLK_PORT, FPGA_CLK_PIN, GPIO_MODE_AF, GPIO_OTYPE_PP, GPIO_SPEED_FREQ_VERY_HIGH, GPIO_NOPULL);
  gpio_set_af(FPGA_CLK_PORT, FPGA_CLK_PIN, GPIO_AF_MCO);

  // buttons
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIOGEN | RCC_AHB1ENR_GPIOJEN | RCC_AHB1ENR_GPIOBEN;
  gpio_init(PWR_PB_PORT, PWR_PB_PIN, GPIO_MODE_IN);
  gpio_init(VOL_DOWN_PB_PORT, VOL_DOWN_PB_PIN, GPIO_MODE_IN);
  gpio_init(VOL_UP_PB_PORT, VOL_UP_PB_PIN, GPIO_MODE_IN);

  // GSM
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIOJEN;
  gpio_init(GSM_WAKEUP_PORT, GSM_WAKEUP_PIN, GPIO_MODE_OUT);
  gpio_reset(GSM_WAKEUP_PORT, GSM_WAKEUP_PIN);
}

void heap_init()
{
  heap_conf(&sys_heap, MEM_HEAP_ADDR, MEM_HEAP_SIZE);
  bsp_ld_print();
}

void fb_init()
{
  // clear areas
  memset(LCD_FB_PTR, 0, LCD_FB_SIZE); // fb
  memset(LCD_FB_PTR + LCD_FB_SIZE, 0, LCD_FB_SIZE); // fb2 (clut)
}

void ltdc_init()
{
  ltdc_conf(LCD_WIDTH, LCD_HEIGHT, LCD_BPP, LCD_FB_ADDR);
}

void sd_init()
{
  if(!gpio_read_pin(SDHC_ST_PORT, SDHC_ST_PIN))
  {
    sdio_init_card();
  }
  else printf("sdio slot not found! \n");
}

void bsp_sys_init()
{
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIOHEN;
  gpio_set(LEDS_PORT, LED_YELLOW_PIN);

  struct
  {
    const char *name;
    void (*fp)();
  } init_table[] = {
    { 0,         rcc_setup_clk },
    { 0,         bsp_io_init },
    { 0,         i2c1_init },
    { 0,         pwr_init },
    { "UART1",   uart1_init },
    { "UART3",   uart3_init },
    { "UART6",   uart6_init },
    { "I2C3",    i2c3_init },
    { "FMC",     fmc_init },
    { "SDRAM",   fmc_sdram_init },
    { "HEAP",    heap_init },
    { "PLL_SAI", rcc_conf_sai_clk},
    { "FB",      fb_init },
    { "LTDC",    ltdc_init },
    { "SDIO",    sdio_init },
    { "SPI1",    spi1_init },
    { "LCD",     lcd_init },
    { "DMA2D",   dma2d_init },
    { "SD",      sd_init },
    { "VFS",     vfs_init },
    { "RNG",     rng_init },
    { "LSE",     rcc_setup_lse },
    { "RTC",     rtc_init },
    { "ADC",     adc_init },
  };

  for(int i = 0; i < SIZE_OF_ARRAY(init_table); i++)
  {
    init_table[i].fp();
    if(init_table[i].name)
    {
      printf("%-10s %s \n", init_table[i].name, "[ OK ]");
    }
  }

  if(RTC_BACKUP_REG(0) != RTC_BACKUP_MAGIC)
  {
    RTC_BACKUP_REG(0) = RTC_BACKUP_MAGIC;

    rtc_dt_t dt = { { 00, 00, 00 }, { 1, 1, 19, 2 } };
    rtc_set_dt(&dt);
  }

  gpio_reset(LEDS_PORT, LED_YELLOW_PIN);
}

#if 0
void spi_mem_test()
{
  uint8_t buf_wr[] = { 1, 2, 3, 4, 5, 6, 7, 8 };
  if(w25q64_write(0, buf_wr, sizeof(buf_wr)))
  {
    printf("write error \n\r");
  }

  printf("wr: %d bytes\n\r", sizeof(buf_wr));

  uint8_t buf_rd[128];
  w25q64_read(0, buf_rd, sizeof(buf_rd));

  printf("rd: \n\r");

  uint32_t i;
  for(i = 0; i < sizeof(buf_rd); i++)
  {
    printf("%02x ", buf_rd[i]);
    if(!((i + 1) % 16)) printf("\n\r", 0);
  }
  printf("\n\r", 0);
}
#endif
