/*
 * bsp.h
 *
 *  Created on: Apr 12, 2019
 *      Author: vchumakov
 */

#ifndef BSP_H_
#define BSP_H_

#include "hw_defs.h"

#define BSP_UNAME "redboot"

#define BSP_DEFAULT_BL 70

#define I2C_DEV_BME280 0x76
#define I2C_DEV_MPU9250 0x69
#define I2C_DEV_TPS65217 0x24
#define I2C_DEV_AK8963 0x0c

// PWR defines
#define BSP_LDO1_VSND 0x02
#define BSP_LDO2_VSENS 0x01
#define BSP_DCDC3_3V3 0x04
#define BSP_DCDC2_4V 0x08
#define BSP_DCDC1_VMCU 0x10
#define BSP_LDO4_1V2 0x20
#define BSP_LDO3_2V5 0x40

#define BSP_PWR_CONF_DEFAULT (BSP_LDO2_VSENS | BSP_DCDC3_3V3 | BSP_DCDC1_VMCU | BSP_DCDC2_4V)

// LEDS
#define LEDS_PORT GPIOH
#define LED_GREEN_PIN 11
#define LED_YELLOW_PIN 12

// PWR
#define PWR_ENABLE_PORT GPIOJ
#define PWR_ENABLE_PIN 12

// EPCS-FPGA
#define EPCS_CS_PORT GPIOJ
#define EPCS_CS_PIN 13

// SDHC
#define SDHC_ST_PORT GPIOK
#define SDHC_ST_PIN 0

// FPGA
#define FPGA_CONF_DONE_PORT GPIOA
#define FPGA_CONF_DONE_PIN 6
#define FPGA_CLK_PORT GPIOA
#define FPGA_CLK_PIN 8

// BUTTONS
#define PWR_PB_PORT GPIOG
#define PWR_PB_PIN 7
#define VOL_DOWN_PB_PORT GPIOI
#define VOL_DOWN_PB_PIN 8
#define VOL_UP_PB_PORT GPIOB
#define VOL_UP_PB_PIN 9

// DGB_MCU_TEST
#define DGB_MCU_TEST_PORT GPIOJ
#define DGB_MCU_TEST_PIN 0

// GSM
#define GSM_WAKEUP_PIN 9
#define GSM_WAKEUP_PORT GPIOJ

// DGB_MCU_TEST
#define ESP_CE_PORT GPIOH
#define ESP_CE_PIN 6

// VBAT
#define VBAT_ADC ADC3

void bsp_cli(char c);
void bsp_cli_str(const char *s);
void bsp_io_init();
void bsp_sys_init();

#endif /* BSP_H_ */
