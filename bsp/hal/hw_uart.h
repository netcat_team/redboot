/*
 * hw_uart.h
 *
 *  Created on: Feb 8, 2017
 *      Author: netcat
 */

#ifndef HW_UART_H_
#define HW_UART_H_

#include "hw_defs.h"

#define UARTS_BR 115200

void uart1_init(void);
void uart3_init(void);
void uart6_init(void);
void uart_out(USART_TypeDef *uart, uint8_t data);
uint32_t uart_in(USART_TypeDef *uart, uint8_t *c);
void uart_io(USART_TypeDef *UARTx, const iovec *vec, uint32_t vec_len, void *rd_buf, uint32_t rd_len);

#endif /* HW_UART_H_ */
