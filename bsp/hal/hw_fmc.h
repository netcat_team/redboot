/*
 * stm_fmc.h
 *
 *  Created on: Feb 8, 2017
 *      Author: netcat
 */

#ifndef HW_FMC_H
#define HW_FMC_H

#include "hw_defs.h"

#define USE_SDRAM_REMAP

#ifdef USE_SDRAM_REMAP
#define SDRAM_ADDR ((uint32_t)0x80000000)
#else
#define SDRAM_ADDR ((uint32_t)0xC0000000)
#endif

#define SDRAM_SIZE (256/8 * 1024 * 1024)
#define SDRAM_WRITE(addr, data) (*(volatile uint32_t*)(SDRAM_ADDR + addr)) = data
#define SDRAM_READ(addr) (*(uint32_t*)(SDRAM_ADDR + addr))

void fmc_init(void);
void fmc_sdram_init(void);
void fmc_sram_init(void);

#endif
