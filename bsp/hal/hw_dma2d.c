/*
 * hw_dma2d.c
 *
 *  Created on: Sep 15, 2017
 *      Author: netcat
 */

#include "hw_gpio.h"
#include "hw_dma2d.h"
#include "hw_rcc.h"

void dma2d_init()
{
  RCC->AHB1ENR |= RCC_AHB1ENR_DMA2EN | RCC_AHB1ENR_DMA2DEN;
}

void dma2d_rst()
{
  RCC->AHB1RSTR |= RCC_AHB1RSTR_DMA2DRST;
  RCC->AHB1RSTR &= ~RCC_AHB1RSTR_DMA2DRST;
}

void dma2d_start()
{
  DMA2D->CR |= DMA2D_CR_START;
}

void dma2d_mode(uint32_t mode)
{
//  DMA2D->CR &= CR_MASK;
  DMA2D->CR |= mode;
  //  DMA2D->OPFCCR &= ~DMA2D_OPFCCR_CM;
  DMA2D->OPFCCR |= DMA2D_RGB565;
}

void dma2d_set_clr(uint32_t clr)
{
  DMA2D->OCOLR = clr;
}

void dma2d_set_clut_color(uint32_t clr)
{
  DMA2D->FGCLUT[0] = clr;
}

void dma2d_cfg(uint32_t output_offset, uint32_t number_of_line, uint32_t pixel_in_line, uint32_t output_addr)
{
  DMA2D->OOR = output_offset;
  DMA2D->NLR = number_of_line | (pixel_in_line << 16);
  DMA2D->OMAR = output_addr;
}

void dma2d_wait()
{
  while(!(DMA2D->ISR & DMA2D_ISR_TCIF));
}

void dma2d_fg_bg_cfg_clut(uint32_t vbuf_from, uint32_t vbuf_to, uint32_t bg_offset, int tp_flag)
{
  // fg
  DMA2D->FGMAR = vbuf_from;
  DMA2D->FGPFCCR = tp_flag ? CM_A8 : CM_L8;
  // bg
  DMA2D->BGMAR = vbuf_to;
  DMA2D->BGPFCCR = CM_RGB565;
  DMA2D->BGOR = bg_offset;
}

void dma2d_fg_bg_cfg_cp(uint32_t vbuf_from, uint32_t vbuf_to, uint32_t bg_offset)
{
  // fg
  DMA2D->FGMAR = vbuf_from;
  DMA2D->FGPFCCR = CM_RGB565 | (1 << 16);
  // bg
  DMA2D->BGMAR = vbuf_to;
  DMA2D->BGPFCCR = CM_RGB565 | (1 << 16);
  DMA2D->BGOR = bg_offset;
}
