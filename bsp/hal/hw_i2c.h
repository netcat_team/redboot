/*
 * hal_i2c.h
 *
 *  Created on: Jul 15, 2017
 *      Author: netcat
 */

#ifndef HAL_HW_I2C_H_
#define HAL_HW_I2C_H_

#include "hw_defs.h"

#define I2C_MAX_STD_SPEED_HZ 100000

void i2c1_init();
void i2c3_init();

//uint8_t i2c_write(uint8_t addr, uint32_t reg, uint8_t reg_addr_size, const uint8_t *buf, uint8_t len);
uint32_t i2c_io(I2C_TypeDef *I2Cx, const iovec *vec, uint32_t vec_len, void *rd_buf, uint32_t rd_len);
void i2c_cfg(I2C_TypeDef* I2Cx, uint32_t freq);
void i2c_errata(I2C_TypeDef *I2Cx, GPIO_TypeDef *GPIOx, uint32_t sda_pin, uint32_t scl_pin);

#define I2C_TOUT(x) \
  tout = 0xffff; \
  while(x) { if(!tout--) goto ret; }

#define I2C_ST(I2Cx, FLAGS) (((I2Cx->SR1 | (I2Cx->SR2 << 16)) & (FLAGS)) == (FLAGS))
#define I2C_FLAGS(F1, F2) ((F1) | ((F2) << 16))

#define I2C_BUSY I2C_FLAGS(0, I2C_SR2_BUSY)
#define I2C_M_START I2C_FLAGS(I2C_SR1_SB, I2C_SR2_MSL | I2C_SR2_BUSY)
#define I2C_M_ADDR_TX I2C_FLAGS(I2C_SR1_ADDR | I2C_SR1_TXE, I2C_SR2_MSL | I2C_SR2_BUSY | I2C_SR2_TRA)
#define I2C_M_ADDR_RX I2C_FLAGS(I2C_SR1_ADDR, I2C_SR2_MSL | I2C_SR2_BUSY)
#define I2C_M_TX I2C_FLAGS(I2C_SR1_BTF | I2C_SR1_TXE, I2C_SR2_MSL | I2C_SR2_BUSY | I2C_SR2_TRA)
#define I2C_M_RX I2C_FLAGS(I2C_SR1_RXNE, I2C_SR2_MSL | I2C_SR2_BUSY)

#endif /* HAL_HW_I2C_H_ */
