/*
 * hw_ltdc.c
 *
 *  Created on: Dec 24, 2018
 *      Author: netcat
 */

#include "stm32f429xx.h"
#include "hal/hw_gpio.h"
#include "hal/hw_ltdc.h"
#include "hal/hw_fmc.h"

const gpio_init_t gpio_ltdc_init[] = {
  // FMC
  { GPIOA, 11 }, // LTDC_R4
  { GPIOA, 12 }, // LTDC_R5

  { GPIOB, 0 }, // LTDC_R3
  { GPIOB, 1 }, // LTDC_R6
  { GPIOB, 8 }, // LTDC_B6

  { GPIOD, 3 }, // LTDC_G7

  { GPIOG, 6 }, // LTDC_R7
  { GPIOG, 10 }, // LTDC_G3

  { GPIOH, 13 }, // LTDC_G2
  { GPIOH, 15 }, // LTDC_G4

  { GPIOJ, 15 }, // LTDC_B3

  { GPIOI, 0 }, // LTDC_G5
  { GPIOI, 1 }, // LTDC_G6
  { GPIOI, 4 }, // LTDC_B4
  { GPIOI, 5 }, // LTDC_B5
  { GPIOI, 9 }, // LTDC_VSYNC
  { GPIOI, 10 }, // LTDC_HSYNC
  { GPIOI, 14 }, // LTDC_CLK

  { GPIOK, 6 }, // LTDC_B7
  { GPIOK, 7 }, // LTDC_DE
  {}};

void ltdc_conf(uint32_t w, uint32_t h, uint32_t bpp, uint32_t fb)
{
  RCC->APB2ENR |= RCC_APB2ENR_LTDCEN;

  // gpio cfg
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN | RCC_AHB1ENR_GPIOBEN | RCC_AHB1ENR_GPIODEN |
                  RCC_AHB1ENR_GPIOGEN | RCC_AHB1ENR_GPIOHEN | RCC_AHB1ENR_GPIOJEN |
                  RCC_AHB1ENR_GPIOIEN | RCC_AHB1ENR_GPIOKEN;

  gpio_init_af(gpio_ltdc_init, GPIO_AF_LTDC);

  // timings
  LTDC->SSCR |= ((LCD_HSYNC - 1) << 16 | (LCD_VSYNC - 1));
  LTDC->BPCR |= ((LCD_HSYNC + LCD_HBP - 1) << 16 | (LCD_VSYNC + LCD_VBP - 1));
  LTDC->AWCR |= ((w + LCD_HSYNC + LCD_HBP - 1) << 16 | (h + LCD_VSYNC + LCD_VBP - 1));
  LTDC->TWCR |= ((w + LCD_HSYNC + LCD_HBP + LCD_HFP - 1) << 16 | (h + LCD_VSYNC + LCD_VBP + LCD_VFP - 1));
  // bg clr
  LTDC->BCCR = 0;

  // win width size
  LTDC_Layer1->WHPCR |= (((w + LCD_HBP + LCD_HSYNC - 1) << 16) | (LCD_HBP + LCD_HSYNC));
  // win height size
  LTDC_Layer1->WVPCR |= (((h + LCD_VSYNC + LCD_VBP - 1) << 16) | (LCD_VSYNC + LCD_VBP));
  // format RGB8888
  LTDC_Layer1->PFCR = PFCR_RGB565;

  // address layer buffer display
  LTDC_Layer1->CFBAR = fb;

  // alpha constant
  LTDC_Layer1->CACR = 255;
  // blending factors
  LTDC_Layer1->BFCR = 0;
  // color frame buffer length
  LTDC_Layer1->CFBLR |= (((bpp * w) << 16) | (bpp * w + 3));
  // frame buffer line number
  LTDC_Layer1->CFBLNR = h;
  // enable layer #2
  LTDC_Layer1->CR |= LTDC_LxCR_LEN;

  // reload
  LTDC->SRCR |= LTDC_SRCR_VBR;
  LTDC->GCR |= LTDC_GCR_LTDCEN;
}
