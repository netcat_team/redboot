/*
 * hw_rng.c
 *
 *  Created on: Apr 29, 2020
 *      Author: netcat
 */

#include "hw_rcc.h"
#include "hw_rng.h"

void rng_init()
{
  RCC->AHB2ENR |= RCC_AHB2ENR_RNGEN;
  RNG->CR |= RNG_CR_RNGEN;
}

uint32_t rng_data()
{
  while(!(RNG->SR & RNG_SR_DRDY));
  return RNG->DR;
}


