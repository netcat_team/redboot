/*
 * hw_rcc.h
 *
 *  Created on: Feb 7, 2017
 *      Author: netcat
 */

#ifndef HW_RCC_H
#define HW_RCC_H

#include "hw_defs.h"

#define HSE_HZ MHZ_TO_HZ(8)
#define RCC_HCLK_HZ MHZ_TO_HZ(168)
#define RCC_APB1_CLK_HZ MHZ_TO_HZ(42)
#define RCC_APB2_CLK_HZ MHZ_TO_HZ(84)

#define RCC_APB1_PS 4
#define RCC_APB2_PS 2

enum
{
  PLL_M = 8,
  PLL_N = 336,
  PLL_P = 2,   // SYSCLK
  PLL_Q = 7    // USB_FS, SDIO, RNG
};

#define PLL_Q_FREQ (((HSE_HZ / PLL_M) * PLL_N) / PLL_Q)

void rcc_setup_lse();
void rcc_setup_clk(void);
void rcc_conf_sai_clk(void);

#endif
