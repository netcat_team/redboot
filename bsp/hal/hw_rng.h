/*
 * hw_rng.h
 *
 *  Created on: Apr 29, 2020
 *      Author: netcat
 */

#ifndef BSP_HAL_HW_RNG_H_
#define BSP_HAL_HW_RNG_H_

#include "hw_defs.h"

void rng_init();
uint32_t rng_data();

#endif /* BSP_HAL_HW_RNG_H_ */
