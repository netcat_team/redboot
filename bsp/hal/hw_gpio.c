/*
 * hw_gpio.c
 *
 *  Created on: Feb 7, 2017
 *      Author: netcat
 */

#include "stm32f429xx.h"
#include "hal/hw_gpio.h"

void gpio_init_ext(GPIO_TypeDef *GPIOx, uint32_t pin, gpio_mode_t mode, gpio_otype_t otype, gpio_speed_t speed, gpio_pupd_t pupd)
{
  uint32_t i = pin;

  GPIOx->OTYPER &= ~(0x01 << i);
  GPIOx->OTYPER |= otype << i;

  i <<= 1;

  GPIOx->MODER &= ~(0x03 << i);
  GPIOx->MODER |= mode << i;

  GPIOx->OSPEEDR &= ~(0x03 << i);
  GPIOx->OSPEEDR |= speed << i;

  GPIOx->PUPDR &= ~(0x03 << i);
  GPIOx->PUPDR |= pupd << i;
}

void gpio_set_af(GPIO_TypeDef *GPIOx, uint32_t pin, gpio_af_t af)
{
  uint32_t i;
  if(pin < 8)
  {
    i = pin << 2;
    GPIOx->AFR[0] &= ~(0x0f << i);
    GPIOx->AFR[0] |= af << i;
  }
  else
  {
    i = (pin - 8) << 2;
    GPIOx->AFR[1] &= ~(0x0f << i);
    GPIOx->AFR[1] |= af << i;
  }
}

void gpio_init_af_ext(const gpio_init_t *array, gpio_af_t af, gpio_otype_t otype, gpio_pupd_t pupd)
{
  while(array->GPIOx)
  {
    gpio_set_af(array->GPIOx, array->pin, af);
    gpio_init_ext(array->GPIOx, array->pin, GPIO_MODE_AF, otype, GPIO_SPEED_FREQ_VERY_HIGH, pupd);
    array++;
  }
}

void gpio_init_af(const gpio_init_t *array, gpio_af_t af)
{
  gpio_init_af_ext(array, af, GPIO_OTYPE_PP, GPIO_NOPULL);
}

void gpio_deinit(const gpio_init_t *array)
{
  while(array->GPIOx)
  {
    gpio_set_af(array->GPIOx, array->pin, GPIO_AF_DEFAULT);
    gpio_init_ext(array->GPIOx, array->pin, GPIO_MODE_IN, GPIO_OTYPE_PP, GPIO_SPEED_FREQ_VERY_HIGH, GPIO_NOPULL);
    array++;
  }
}

void gpio_init(GPIO_TypeDef *GPIOx, uint32_t pin, gpio_mode_t mode)
{
  gpio_init_ext(GPIOx, pin, mode, GPIO_OTYPE_PP, GPIO_SPEED_FREQ_VERY_HIGH, GPIO_NOPULL);
}
