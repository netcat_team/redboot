/*
 * rtc.c
 *
 *  Created on: Jul 23, 2019
 *      Author: netcat
 */

#include "hw_rtc.h"
#include "sys/cli/syslog.h"

RTC_TypeDef *RTCx = RTC;

static void rtc_wp(uint32_t wp)
{
  if(wp)
  {
    // init mode dis
    RTCx->ISR &= ~RTC_ISR_INIT;
    // WP en
    RTCx->WPR = RTC_WP_EN;
  }
  else
  {
    // WP dis
    RTCx->WPR = RTC_WP_DIS_KEY1;
    RTCx->WPR = RTC_WP_DIS_KEY2;

    // init mode en
    RTCx->ISR |= RTC_ISR_INIT;
    while(!(RTC->ISR & RTC_ISR_INITF));
  }
}

void rtc_init()
{
  // RTC en
  RCC->BDCR |= RCC_BDCR_RTCEN;

  rtc_wp(0);

  // 24h, no output
  RTCx->CR = 0;
  RTCx->PRER = RTC_SYNCH_PS_DEFAULT | (RTC_ASYNCH_PS_DEFAULT << 16);

  rtc_wp(1);
}

void rtc_set_dt(rtc_dt_t *dt)
{
  uint32_t time_bcd = 0;
  time_bcd |= BIN_TO_BCD(dt->time.hour) << RTC_TR_HU_Pos;
  time_bcd |= BIN_TO_BCD(dt->time.min) << RTC_TR_MNU_Pos;
  time_bcd |= BIN_TO_BCD(dt->time.sec) << RTC_TR_SU_Pos;
  time_bcd &= RTC_TR_MASK;

  uint32_t date_bcd = 0;
  date_bcd |= BIN_TO_BCD(dt->date.year) << RTC_DR_YU_Pos;
  date_bcd |= dt->date.wd << RTC_DR_WDU_Pos;
  date_bcd |= BIN_TO_BCD(dt->date.month) << RTC_DR_MU_Pos;
  date_bcd |= BIN_TO_BCD(dt->date.day) << RTC_DR_DU_Pos;
  date_bcd &= RTC_DR_MASK;

  rtc_wp(0);
  RTCx->TR = time_bcd;
  RTCx->DR = date_bcd;
  rtc_wp(1);
}

void rtc_dt(rtc_dt_t *dt)
{
  uint32_t time_bcd = RTCx->TR & RTC_TR_MASK;
  uint32_t date_bcd = RTCx->DR & RTC_DR_MASK;

  dt->time.hour = BCD_TO_BIN((time_bcd & (RTC_TR_HT | RTC_TR_HU)) >> RTC_TR_HU_Pos);
  dt->time.min = BCD_TO_BIN((time_bcd & (RTC_TR_MNT | RTC_TR_MNU)) >> RTC_TR_MNU_Pos);
  dt->time.sec = BCD_TO_BIN(time_bcd & (RTC_TR_ST | RTC_TR_SU)) >> RTC_TR_SU_Pos;

  dt->date.year = BCD_TO_BIN((date_bcd & (RTC_DR_YT | RTC_DR_YU)) >> RTC_DR_YU_Pos);
  dt->date.wd = (date_bcd & RTC_DR_WDU) >> RTC_DR_WDU_Pos;
  dt->date.month = BCD_TO_BIN((date_bcd & (RTC_DR_MT | RTC_DR_MU)) >> RTC_DR_MU_Pos);
  dt->date.day = BCD_TO_BIN((date_bcd & (RTC_DR_DT | RTC_DR_DU)) >> RTC_DR_DU_Pos);
}

