/*
 * hw_ltdc.h
 *
 *  Created on: Dec 24, 2018
 *      Author: netcat
 */

#ifndef HW_LTDC_H
#define HW_LTDC_H

#include "hw_defs.h"
#include "hw_fmc.h"

/*
* HSW = (LCD_HSYNC - 1)
* VSH = (LCD_VSYNC - 1)
* AHBP = (LCD_HSYNC + LCD_HBP - 1)
* AVBP = (LCD_VSYNC + LCD_VBP - 1)
* AAW = (LCD_HEIGHT + LCD_VSYNC + LCD_VBP - 1)
* AAH = (LCD_WIDTH + LCD_HSYNC + LCD_HBP - 1)
*/
#define LCD_HSYNC 20 // Horizontal synchronization
#define LCD_HBP 1 // Horizontal back porch
#define LCD_HFP 1 // Horizontal front porch

#define LCD_VSYNC 50 // Vertical synchronization
#define LCD_VBP 1 // Vertical back porch
#define LCD_VFP 1 // Vertical front porch

enum
{
  PFCR_ARGB8888,
  PFCR_RGB888,
  PFCR_RGB565,
  PFCR_ARGB1555,
  PFCR_ARGB4444,
  PFCR_L8,
  PFCR_AL44,
  PFCR_AL88
};

void ltdc_conf(uint32_t w, uint32_t h, uint32_t bpp, uint32_t fb);

#endif
