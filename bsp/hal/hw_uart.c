/*
 * hw_uart.c
 *
 *  Created on: Feb 8, 2017
 *      Author: netcat
 */

#include "hal/hw_gpio.h"
#include "hal/hw_rcc.h"
#include "hal/hw_uart.h"

static void usart_config(USART_TypeDef *UARTx, uint32_t br, uint32_t rx_ie)
{
  // cfg
  UARTx->CR1 = USART_CR1_TE | USART_CR1_RE;
  UARTx->CR2 = 0;
  UARTx->CR3 = 0;

  if(rx_ie) UARTx->CR1 |= USART_CR1_RXNEIE; // rx ie
  UARTx->BRR = br;

  // en
  UARTx->CR1 |= USART_CR1_UE;
}

void uart1_init()
{
  // gpio cfg
  const gpio_init_t gpio_uart1_init[] = {
    { GPIOA, 9 }, // TX
    { GPIOA, 10 }, // RX
    {}};
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;
  gpio_init_af(gpio_uart1_init, GPIO_AF_USART1);

  // clk en
  RCC->APB2ENR |= RCC_APB2ENR_USART1EN;

  usart_config(USART1, DIV_INT_ROUND(RCC_APB2_CLK_HZ, UARTS_BR), 1);
}

void uart3_init()
{
  // gpio cfg
  const gpio_init_t gpio_uart3_init[] = {
    { GPIOB, 10 }, // TX
    { GPIOB, 11 }, // RX
    {}};
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN;
  gpio_init_af(gpio_uart3_init, GPIO_AF_USART3);
  // clk en
  RCC->APB1ENR |= RCC_APB1ENR_USART3EN;

  usart_config(USART3, DIV_INT_ROUND(RCC_APB1_CLK_HZ, UARTS_BR), 1);
}

void uart6_init()
{
  // gpio cfg
  const gpio_init_t gpio_uart6_init[] = {
    { GPIOC, 6 }, // TX
    { GPIOC, 7 }, // RX
    {}};
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN;
  gpio_init_af(gpio_uart6_init, GPIO_AF_USART6);
  // clk en
  RCC->APB2ENR |= RCC_APB2ENR_USART6EN;

  usart_config(USART6, DIV_INT_ROUND(RCC_APB2_CLK_HZ, UARTS_BR), 1);
}

void uart_out(USART_TypeDef *UARTx, uint8_t data)
{
  while(!(UARTx->SR & USART_SR_TXE));
  UARTx->DR = data;
}

uint32_t uart_in(USART_TypeDef *UARTx, uint8_t *c)
{
  uint32_t ret = UARTx->SR & USART_SR_RXNE;
  if(ret)
  {
    UARTx->SR &= ~USART_SR_RXNE;
    *c = UARTx->DR;
  }
  return ret;
}

void uart_io(USART_TypeDef *UARTx, const iovec *vec, uint32_t vec_len, void *rd_buf, uint32_t rd_len)
{
  uint8_t *rdb = rd_buf;

  uint32_t i, j;
  for(i = 0; i < vec_len; i++)
    for(j = 0; j < vec[i].len; j++)
    {
      while(!(UARTx->SR & USART_SR_TXE));
      UARTx->DR = ((uint8_t*)vec[i].data)[j];
    }

  // return to RX mode only after end of transaction
  while(!(UARTx->SR & USART_SR_TC));

  while(rd_len--)
  {
    while(!(UARTx->SR & USART_SR_RXNE));
    if(rdb) *rdb++ = UARTx->DR;
  }
}
