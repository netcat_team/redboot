/*
 * hw_rtc.h
 *
 *  Created on: Jul 23, 2019
 *      Author: netcat
 */

#ifndef BSP_HAL_HW_RTC_H_
#define BSP_HAL_HW_RTC_H_

#include "hw_defs.h"

// PS defaults
#define RTC_ASYNCH_PS_DEFAULT 0x7F
#define RTC_SYNCH_PS_DEFAULT 0xFF

// WP keys
#define RTC_WP_EN 0xFF
#define RTC_WP_DIS_KEY1 0xCA
#define RTC_WP_DIS_KEY2 0x53

// time & date masks
#define RTC_TR_MASK (RTC_TR_HT | RTC_TR_HU | RTC_TR_MNT | RTC_TR_MNU | RTC_TR_ST | RTC_TR_SU)
#define RTC_DR_MASK (RTC_DR_WDU | RTC_DR_MT | RTC_DR_MU | RTC_DR_DT | RTC_DR_DU | RTC_DR_YT | RTC_DR_YU)

// backup domain
#define RTC_BACKUP_REG(reg) (*(volatile uint32_t *)((&RTC->BKP0R) + (reg)))
#define RTC_BACKUP_MAGIC 0x32F2

typedef struct
{
  uint8_t hour;
  uint8_t min;
  uint8_t sec;
  uint8_t ms;
} rtc_time_t;

typedef struct
{
  uint8_t day;
  uint8_t month;
  uint8_t year;
  uint8_t wd;
} rtc_date_t;

typedef struct
{
  rtc_time_t time;
  rtc_date_t date;
} rtc_dt_t;

void rtc_init();
void rtc_set_dt(rtc_dt_t *dt);
void rtc_dt(rtc_dt_t *dt);

#endif /* BSP_HAL_HW_RTC_H_ */
