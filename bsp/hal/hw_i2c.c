
#include "hw_gpio.h"
#include "hw_i2c.h"
#include "hw_rcc.h"
#include "syslog.h"
#include "printf.h"

void i2c_errata(I2C_TypeDef *I2Cx, GPIO_TypeDef *GPIOx, uint32_t sda_pin, uint32_t scl_pin)
{
  // 1. Clear PE bit
  I2Cx->CR1 &= ~I2C_CR1_PE;

  // 2. Configure the SCL and SDA I/Os as General Purpose Output Open-Drain, High level (Write 1 to GPIOx_ODR)
  gpio_set_af(GPIOx, scl_pin, GPIO_AF_DEFAULT);
  gpio_init_ext(GPIOx, scl_pin, GPIO_MODE_OUT, GPIO_OTYPE_OD, GPIO_SPEED_FREQ_HIGH, GPIO_NOPULL);
  gpio_set_af(GPIOx, sda_pin, GPIO_AF_DEFAULT);
  gpio_init_ext(GPIOx, sda_pin, GPIO_MODE_OUT, GPIO_OTYPE_OD, GPIO_SPEED_FREQ_HIGH, GPIO_NOPULL);

  gpio_write_pin(GPIOx, scl_pin, GPIO_PIN_SET);
  gpio_write_pin(GPIOx, sda_pin, GPIO_PIN_SET);

  // 3. Check SCL and SDA High level in GPIOx_IDR
  while(!gpio_read_pin(GPIOx, scl_pin));
  while(!gpio_read_pin(GPIOx, sda_pin));

  // 4. Configure the SDA I/O as General Purpose Output Open-Drain, Low level (Write 0 to GPIOx_ODR)
  gpio_write_pin(GPIOx, sda_pin, GPIO_PIN_RESET);

  // 5. Check SDA Low level in GPIOx_IDR
  while(gpio_read_pin(GPIOx, sda_pin));

  // 6. Configure the SCL I/O as General Purpose Output Open-Drain, Low level (Write 0 to GPIOx_ODR)
  gpio_write_pin(GPIOx, scl_pin, GPIO_PIN_RESET);

  // 7. Check SCL Low level in GPIOx_IDR
  while(gpio_read_pin(GPIOx, scl_pin));

  // 8. Configure the SCL I/O as General Purpose Output Open-Drain, High level (Write 1 to GPIOx_ODR)
  gpio_write_pin(GPIOx, scl_pin, GPIO_PIN_SET);

  // 9. Check SCL High level in GPIOx_IDR
  while(!gpio_read_pin(GPIOx, scl_pin));

  // 10. Configure the SDA I/O as General Purpose Output Open-Drain , High level (Write 1 to GPIOx_ODR)
  gpio_write_pin(GPIOx, sda_pin, GPIO_PIN_SET);

  // 11. Check SDA High level in GPIOx_IDR
  while(!gpio_read_pin(GPIOx, sda_pin));

  // 12. Configure the SCL and SDA I/Os as Alternate function Open-Drain
  gpio_set_af(GPIOx, scl_pin, GPIO_AF_I2C1);
  gpio_init_ext(GPIOx, scl_pin, GPIO_MODE_AF, GPIO_OTYPE_OD, GPIO_SPEED_FREQ_HIGH, GPIO_NOPULL);
  gpio_set_af(GPIOx, sda_pin, GPIO_AF_I2C1);
  gpio_init_ext(GPIOx, sda_pin, GPIO_MODE_AF, GPIO_OTYPE_OD, GPIO_SPEED_FREQ_HIGH, GPIO_NOPULL);

  // 13. Set SWRST bit in I2Cx_CR1 register
  I2Cx->CR1 |= I2C_CR1_SWRST;
  asm("nop");

  // 14. Clear SWRST bit in I2Cx_CR1 register
  I2Cx->CR1 &= ~I2C_CR1_SWRST;
  asm("nop");

  // 15. Enable the I2C peripheral by setting the PE bit in I2Cx_CR1 register
  I2Cx->CR1 |= I2C_CR1_PE;
}

void i2c_cfg(I2C_TypeDef* I2Cx, uint32_t freq)
{
  // sw reset
  I2Cx->CR1 |= I2C_CR1_SWRST;
  I2Cx->CR1 &= ~I2C_CR1_SWRST;

  // setup freq
  uint32_t f_range = HZ_TO_MHZ(RCC_APB1_CLK_HZ);
  I2Cx->CR2 = f_range;

  if(freq <= I2C_MAX_STD_SPEED_HZ)
  {
    I2Cx->TRISE = f_range + 1;
    I2Cx->CCR = MAX(4, RCC_APB1_CLK_HZ / (freq << 1));
  }
  else
  {
    I2Cx->TRISE = ((f_range * 300) / 1000) + 1;
    I2Cx->CCR = I2C_CCR_FS | MAX(1, (RCC_APB1_CLK_HZ / (freq * 3)) & I2C_CCR_CCR);
  }

  // enable
  I2Cx->CR1 |= I2C_CR1_PE;
}

void i2c1_init()
{
  // gpio cfg
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN;

  const gpio_init_t gpio_i2c1_init[] = {
    { GPIOB, 6 }, // SCL
    { GPIOB, 7 }, // SDA
    {}};

  gpio_init_af_ext(gpio_i2c1_init, GPIO_AF_I2C1, GPIO_OTYPE_OD, GPIO_NOPULL);

  // clk en
  RCC->APB1ENR |= RCC_APB1ENR_I2C1EN;

  // hw rst
  RCC->APB1RSTR |= RCC_APB1RSTR_I2C1RST;
  RCC->APB1RSTR &= ~RCC_APB1RSTR_I2C1RST;

  i2c_cfg(I2C1, I2C_MAX_STD_SPEED_HZ);
}

void i2c3_init()
{
  // gpio cfg
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIOHEN;

  const gpio_init_t gpio_i2c3_init[] = {
    { GPIOH, 7 }, // SCL
    { GPIOH, 8 }, // SDA
    {}};

  gpio_init_af_ext(gpio_i2c3_init, GPIO_AF_I2C3, GPIO_OTYPE_OD, GPIO_NOPULL);

  // clk en
  RCC->APB1ENR |= RCC_APB1ENR_I2C3EN;

  // hw rst
  RCC->APB1RSTR |= RCC_APB1RSTR_I2C3RST;
  RCC->APB1RSTR &= ~RCC_APB1RSTR_I2C3RST;

  i2c_cfg(I2C3, I2C_MAX_STD_SPEED_HZ);
}

#if 0
void i2c_dump_regs(I2C_TypeDef* I2Cx)
{
  syslog(SYSLOG_DEBUG, "% 7s: %04x", "CR1", I2Cx->CR1);
  syslog(SYSLOG_DEBUG, "% 7s: %04x", "CR2", I2Cx->CR2);
  syslog(SYSLOG_DEBUG, "% 7s: %04x", "TRISE", I2Cx->TRISE);
  syslog(SYSLOG_DEBUG, "% 7s: %04x", "CCR", I2Cx->CCR);
  syslog(SYSLOG_DEBUG, "% 7s: %04x", "SR1", I2Cx->SR1);
  syslog(SYSLOG_DEBUG, "% 7s: %04x", "SR2", I2Cx->SR2);
}

uint32_t i2c_read(uint8_t addr, uint32_t reg, uint8_t reg_addr_size, uint8_t *buf, uint32_t len)
{
  uint32_t tout;
  I2C_TypeDef *I2Cx = I2C1;

  // start
  I2Cx->CR1 |= I2C_CR1_START;
  I2C_TOUT(!I2C_ST(I2Cx, I2C_M_START));

  // addr
  I2Cx->DR = addr & 0xfe;
  I2C_TOUT(!I2C_ST(I2Cx, I2C_M_ADDR_TX));

  // mem addr
  if(reg_addr_size == 4)
  {
    I2Cx->DR = reg >> 24;
    I2C_TOUT(!I2C_ST(I2Cx, I2C_M_TX));
    I2Cx->DR = reg >> 16;
    I2C_TOUT(!I2C_ST(I2Cx, I2C_M_TX));
    I2Cx->DR = reg >> 8;
    I2C_TOUT(!I2C_ST(I2Cx, I2C_M_TX));
  }
  else if(reg_addr_size == 2)
  {
    I2Cx->DR = reg >> 8;
    I2C_TOUT(!I2C_ST(I2Cx, I2C_M_TX));
  }
  I2Cx->DR = reg;
  I2C_TOUT(!I2C_ST(I2Cx, I2C_M_TX));

  // restart
  I2Cx->CR1 |= I2C_CR1_START;
  I2C_TOUT(!I2C_ST(I2Cx, I2C_M_START));

  // send Address
  I2Cx->DR = addr | 0x01;
  I2C_TOUT(!I2C_ST(I2Cx, I2C_M_ADDR_RX));

  // recv data
  I2Cx->CR1 |= I2C_CR1_ACK;
  while(len--)
  {
    I2C_TOUT(!I2C_ST(I2Cx, I2C_M_RX));
    *buf++ = I2Cx->DR;
  }
  I2Cx->CR1 &= ~I2C_CR1_ACK;

  // wait for stop
  I2Cx->CR1 |= I2C_CR1_STOP;
  return 0;

ret:
  I2Cx->CR1 |= I2C_CR1_STOP;
  return 1;
}

uint8_t i2c_write(uint8_t addr, uint32_t reg, uint8_t reg_addr_size, const uint8_t *buf, uint8_t rd_len)
{
  uint32_t tout;
  I2C_TypeDef *I2Cx = I2C1;

  // start
  I2Cx->CR1 |= I2C_CR1_START;
  I2C_TOUT(!I2C_ST(I2Cx, I2C_M_START));

  // dev addr
  I2Cx->DR = addr & 0xfe;
  I2C_TOUT(!I2C_ST(I2Cx, I2C_M_ADDR_TX));

  // mem addr
  if(reg_addr_size == 4)
  {
    I2Cx->DR = reg >> 24;
    I2C_TOUT(!I2C_ST(I2Cx, I2C_M_TX));
    I2Cx->DR = reg >> 16;
    I2C_TOUT(!I2C_ST(I2Cx, I2C_M_TX));
    I2Cx->DR = reg >> 8;
    I2C_TOUT(!I2C_ST(I2Cx, I2C_M_TX));
  }
  else if(reg_addr_size == 2)
  {
    I2Cx->DR = reg >> 8;
    I2C_TOUT(!I2C_ST(I2Cx, I2C_M_TX));
  }
  I2Cx->DR = reg;
  I2C_TOUT(!I2C_ST(I2Cx, I2C_M_TX));

  // send data
  while(rd_len--)
  {
    I2Cx->DR = *buf++;
    I2C_TOUT(!I2C_ST(I2Cx, I2C_M_TX));
  }

  I2Cx->CR1 |= I2C_CR1_STOP;
  return 0;

ret:
  I2Cx->CR1 |= I2C_CR1_STOP;
  return 1;
}
#endif

uint32_t i2c_io(I2C_TypeDef *I2Cx, const iovec *vec, uint32_t vec_len, void *rd_buf, uint32_t rd_len)
{
  uint32_t tout;
  uint8_t *rdb = rd_buf;

  uint8_t dev_addr = ((uint8_t*)vec[0].data)[0] << 1;

  // start
  I2Cx->CR1 |= I2C_CR1_START;
  I2C_TOUT(!I2C_ST(I2Cx, I2C_M_START));

  // dev addr
  I2Cx->DR = dev_addr & 0xfe;
  I2C_TOUT(!I2C_ST(I2Cx, I2C_M_ADDR_TX));

  // send internal addr & data
  uint32_t i, j;
  for(i = 1; i < vec_len; i++)
    for(j = 0; j < vec[i].len; j++)
    {
      I2Cx->DR = ((uint8_t*)vec[i].data)[j];
      I2C_TOUT(!I2C_ST(I2Cx, I2C_M_TX));
    }

  if(rd_len)
  {
    // restart
    I2Cx->CR1 |= I2C_CR1_START;
    I2C_TOUT(!I2C_ST(I2Cx, I2C_M_START));

    // send Address
    I2Cx->DR = dev_addr | 0x01;
    I2C_TOUT(!I2C_ST(I2Cx, I2C_M_ADDR_RX));

    // recv data
    I2Cx->CR1 |= I2C_CR1_ACK;
    while(rd_len--)
    {
      I2C_TOUT(!I2C_ST(I2Cx, I2C_M_RX));
      *rdb++ = I2Cx->DR;
    }
    I2Cx->CR1 &= ~I2C_CR1_ACK;
  }

  I2Cx->CR1 |= I2C_CR1_STOP;
  return 0;

ret:
  I2Cx->CR1 |= I2C_CR1_STOP;
  return 1;
}

