/*
 * hw_rcc.c
 *
 *  Created on: Feb 7, 2017
 *      Author: netcat
 */

#include "hal/hw_rcc.h"
#include "sys/cli/syslog.h"

void rcc_setup_lse()
{
  // LSE en
  RCC->BDCR |= RCC_BDCR_LSEON;
  while(!(RCC->BDCR & RCC_BDCR_LSERDY));

  // use LSE
  RCC->BDCR &= ~RCC_BDCR_RTCSEL;
  RCC->BDCR |= RCC_BDCR_RTCSEL_0;
}

void rcc_setup_clk()
{
  // configure flash prefetch, instruction cache, data cache and wait state
  FLASH->ACR = FLASH_ACR_ICEN | FLASH_ACR_DCEN | FLASH_ACR_PRFTEN | FLASH_ACR_LATENCY_5WS;

  RCC->APB1ENR |= RCC_APB1ENR_PWREN;
  // select regulator voltage output Scale 2
  PWR->CR |= PWR_CR_VOS_1;
  // enable access to backup domain
  PWR->CR |= PWR_CR_DBP;

  // enable the over-drive to extend the clock frequency to 180 MHz
  PWR->CR |= PWR_CR_ODEN;
  while(!(PWR->CSR & PWR_CSR_ODRDY));
  PWR->CR |= PWR_CR_ODSWEN;
  while(!(PWR->CSR & PWR_CSR_ODSWRDY));

  // enable HSE
  RCC->CR |= RCC_CR_HSEON;
  while(!(RCC->CR & RCC_CR_HSERDY));

  // setup PS
  uint32_t t = RCC->CFGR;
  t &= ~(RCC_CFGR_HPRE | RCC_CFGR_PPRE1 | RCC_CFGR_PPRE2);
  t |= RCC_CFGR_HPRE_DIV1 | RCC_CFGR_PPRE1_DIV4 | RCC_CFGR_PPRE2_DIV2;
  RCC->CFGR = t;

  RCC->PLLCFGR = PLL_M | (PLL_N << 6) | (((PLL_P >> 1) - 1) << 16) | RCC_PLLCFGR_PLLSRC_HSE | (PLL_Q << 24);

  // enable the main PLL
  RCC->CR |= RCC_CR_PLLON;
  while(!(RCC->CR & RCC_CR_PLLRDY));

  // use PLL
  RCC->CFGR &= ~RCC_CFGR_SW;
  RCC->CFGR |= RCC_CFGR_SW_PLL;
  while((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_PLL);

  // SysTick: en, HCLK, 1ms
  SysTick->LOAD = (RCC_HCLK_HZ / 1000 - 1);
  SysTick->VAL = 0;
  SysTick->CTRL = SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_ENABLE_Msk;

  // MCO1
  RCC->CFGR &= ~(RCC_CFGR_MCO1 | RCC_CFGR_MCO1);
  RCC->CFGR |= RCC_CFGR_MCO1_0 | RCC_CFGR_MCO1_0 // PLL src
      | RCC_CFGR_MCO1PRE_1 | RCC_CFGR_MCO1PRE_2; // div 4

//  RCC->CFGR |= (3 << RCC_CFGR_MCO1_Pos) // PLL src
//      | (6 < RCC_CFGR_MCO1PRE_Pos); // div 4
}

void rcc_conf_sai_clk()
{
  // pll sai
  RCC->PLLSAICFGR |= 240 << RCC_PLLSAICFGR_PLLSAIN_Pos; // PLL_N
  RCC->PLLSAICFGR |= 4 << RCC_PLLSAICFGR_PLLSAIR_Pos; // PLL_R
  RCC->DCKCFGR &= ~RCC_DCKCFGR_PLLSAIDIVR; // R

  // enable sai
  RCC->CR |= RCC_CR_PLLSAION;
  while(!(RCC->CR & RCC_CR_PLLSAIRDY));
}

