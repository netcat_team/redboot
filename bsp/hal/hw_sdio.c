/*
 * hw_sdio.c
 *
 *  Created on: Apr 3, 2019
 *      Author: vchumakov
 */

#include "hw_sdio.h"
#include "hw_gpio.h"
#include "stm32f429xx.h"
#include "sys/cli/syslog.h"

typedef struct
{
  uint32_t type;
  uint8_t cid[16];
  uint32_t rca;
  uint8_t csd[16];
  uint8_t scr[8];
} sd_card_t;

sd_card_t card = { .type = CARD_UNKNOWN };

const gpio_init_t gpio_sdio_init[] = {
  { GPIOC, 8 }, // D0
  { GPIOC, 9 }, // D1
  { GPIOC, 10 }, // D2
  { GPIOC, 11 }, // D3
  { GPIOC, 12 }, // CLK
  { GPIOD, 2 }, // CMD
  {}};

int sdio_resp(uint32_t cmd, uint32_t mode, uint32_t *buf)
{
  // wait for status
  volatile uint32_t wait = 0x10000;
  uint32_t st = SDIO_STA_CTIMEOUT;
  if(mode) st |= SDIO_STA_CCRCFAIL | SDIO_STA_CMDREND;
  else st |= SDIO_STA_CMDSENT;

  while(!(SDIO->STA & st) && --wait);

  // clear the all flags
  SDIO->ICR = SDIO_ICR_CCRCFAILC | SDIO_ICR_DCRCFAILC | SDIO_ICR_CTIMEOUTC |
        SDIO_ICR_DTIMEOUTC | SDIO_ICR_TXUNDERRC | SDIO_ICR_RXOVERRC |
        SDIO_ICR_CMDRENDC | SDIO_ICR_CMDSENTC | SDIO_ICR_DATAENDC |
        SDIO_ICR_DBCKENDC;

  uint32_t r;
  switch(mode)
  {
    case 1:
      if(SDIO->RESP1 & 0xfdffe008) return -1; // OCR all errors
      break;

    case 2:
      *buf++ = SDIO->RESP1;
      *buf++ = SDIO->RESP2;
      *buf++ = SDIO->RESP3;
      *buf++ = SDIO->RESP4;
      break;

    case 6:
      if(SDIO->RESPCMD != cmd) return -1; // illegal cmd?
      r = SDIO->RESP1;
      if(r & 0xE000) return -1; // R6 all errors
      *buf = r >> 16;
      break;

    case 3:
      *buf++ = SDIO->RESP1;
      break;

    default:
      break;
  }

  return 0;
}

void sdio_cmd(uint32_t cmd, uint32_t arg, uint32_t resp_type)
{
//  if(cmd != 55 && cmd != 41 && cmd != 1) syslog(SYSLOG_DEBUG, "cmd: %d", cmd);
  SDIO->ICR = SDIO_ICR_CMD;
  SDIO->ARG = arg;
  SDIO->CMD = cmd | resp_type | SDIO_CMD_CPSMEN;
}

uint32_t sdio_read_scr(uint32_t *buf)
{
  uint32_t err = SDIO_OK;

  sdio_cmd(SD_CMD_SET_BLOCKLEN, 8, SDIO_RESP_SHORT); // CMD16
  err = sdio_resp(SD_CMD_SET_BLOCKLEN, 1, 0);
  if(err != SDIO_OK) return err;

  // clear the data SDIO flags
  SDIO->ICR = SDIO_ICR_DATA;

  SDIO->DTIMER = SDIO_READ_TIMEOUT;
  SDIO->DLEN = 8;
  // mode: block
  // dir: to card
  // dma: dis
  // block size: 8 (2^9)
  // DPSM: en
  SDIO->DCTRL = SDIO_DCTRL_DTDIR | SDIO_DCTRL_DTEN | (3 << 4);

  sdio_cmd(SD_CMD_APP_CMD, card.rca << 16, SDIO_RESP_SHORT); // CMD55
  err = sdio_resp(SD_CMD_APP_CMD, 1, 0);
  if(err != SDIO_OK) return err;

  sdio_cmd(SD_CMD_SEND_SCR, 0, SDIO_RESP_SHORT); // ACMD51
  err = sdio_resp(SD_CMD_SEND_SCR, 1, 0);
  if(err != SDIO_OK) return err;

  while(!(SDIO->STA & (SDIO_STA_RXOVERR | SDIO_STA_DCRCFAIL | SDIO_STA_DTIMEOUT | SDIO_STA_DBCKEND | SDIO_STA_STBITERR)))
  {
    if(SDIO->STA & SDIO_STA_RXDAVL) *buf++ = SDIO->FIFO;
  }

  // check for errors
  if(SDIO->STA & (SDIO_STA_DTIMEOUT | SDIO_STA_DCRCFAIL | SDIO_STA_STBITERR | SDIO_STA_RXOVERR)) return -1;
  SDIO->ICR = SDIO_ICR_STATIC;

  return err;
}

uint32_t sdio_set_bw4()
{
  uint32_t err = SDIO_OK;

  if(card.type != CARD_MMC)
  {
    sdio_cmd(SD_CMD_APP_CMD, card.rca << 16, SDIO_RESP_SHORT); // CMD55
    err = sdio_resp(SD_CMD_APP_CMD, 1, 0);
    if(err != SDIO_OK) return err;

    sdio_cmd(SD_CMD_SET_BUS_WIDTH, 0x02, SDIO_RESP_SHORT); // ACMD6
    err = sdio_resp(SD_CMD_SET_BUS_WIDTH, 1, 0);
    if(err != SDIO_OK) return err;
  }

  uint32_t t = SDIO->CLKCR;
  t &= ~SDIO_CLKCR_WIDBUS;
  t |= SDIO_CLKCR_WIDBUS_0;
  SDIO->CLKCR = t;

  return err;
}

uint32_t sdio_init_card()
{
  volatile uint32_t wait;
  uint32_t err = SDIO_OK;

  sdio_cmd(SD_CMD_GO_IDLE_STATE, 0, SDIO_RESP_NONE); // CMD0
  err = sdio_resp(SD_CMD_GO_IDLE_STATE, 0, 0);
  if(err != SDIO_OK) return err;

  sdio_cmd(SD_CMD_HS_SEND_EXT_CSD, 0x1aa, SDIO_RESP_SHORT); // CMD8
  err = sdio_resp(SD_CMD_HS_SEND_EXT_CSD, 7, 0);

  uint32_t sdhc_flag = 0;
  if(err == SDIO_OK) sdhc_flag = 0x40000000;

  // issue CMD55 to reset 'illegal command'
  if(!sdhc_flag)
  {
    sdio_cmd(SD_CMD_APP_CMD, 0, SDIO_RESP_SHORT);
    sdio_resp(SD_CMD_APP_CMD, 1, 0);
  }

  wait = 0x1000;
  while(--wait)
  {
      sdio_cmd(SD_CMD_APP_CMD, 0, SDIO_RESP_SHORT); // CMD55 with RCA 0
      err = sdio_resp(SD_CMD_APP_CMD, 1, 0);
      if(err != SDIO_OK) return err;

      sdio_cmd(SD_CMD_SD_APP_OP_COND, 0x80100000 | sdhc_flag, SDIO_RESP_SHORT); // ACMD41
      uint32_t r = 0;
      err = sdio_resp(SD_CMD_SD_APP_OP_COND, 3, &r);

      if(r & (1 << 31))
      {
        if(sdhc_flag)
        {
          if(r & sdhc_flag) card.type = CARD_SDHC;
          else card.type = CARD_SDSC_V2;
        }
        else card.type = CARD_SDSC_V1;

        err = SDIO_OK;
        break;
      }
  }
  if(!wait)
  {
    wait = 0x1000;
    while(--wait)
    {
      sdio_cmd(SD_CMD_SEND_OP_COND, 0x80100000, SDIO_RESP_SHORT); // CMD1
      uint32_t r = 0;
      err = sdio_resp(SD_CMD_SEND_OP_COND, 3, &r);

      if(r & (1 << 31))
      {
    	  card.type = CARD_MMC;
          err = SDIO_OK;
          break;
      }
    }
    if(!wait) return -1;
  }

  syslog(SYSLOG_DEBUG, "card type: %x", card.type);

  // get cid
  sdio_cmd(SD_CMD_ALL_SEND_CID, 0, SDIO_RESP_LONG); // CMD2
  err = sdio_resp(SD_CMD_ALL_SEND_CID, 2, (uint32_t*)card.cid);
  if(err != SDIO_OK) return err;

  if(card.type != CARD_MMC)
  {
    // get rca, card in stand-by state
    sdio_cmd(SD_CMD_SET_REL_ADDR, 0, SDIO_RESP_SHORT); // CMD3
    err = sdio_resp(SD_CMD_SET_REL_ADDR, 6, (uint32_t*)&card.rca);
    if(err != SDIO_OK) return err;
  }

  // get rca reg
  sdio_cmd(SD_CMD_SEND_CSD, card.rca << 16, SDIO_RESP_LONG); // CMD9
  err = sdio_resp(SD_CMD_SEND_CSD, 2, (uint32_t*)card.csd);
  if(err != SDIO_OK) return err;

  // reconf clock
  SDIO->CLKCR = SDIO_CLKCR_CLKEN | SDIO_CLK_DIV_HI;

  // transfer mode
  sdio_cmd(SD_CMD_SEL_DESEL_CARD, card.rca << 16, SDIO_RESP_SHORT); // CMD7
  err = sdio_resp(SD_CMD_SEL_DESEL_CARD, 1, 0);
  if(err != SDIO_OK) return err;

//  sdio_cmd(SD_CMD_APP_CMD, card.rca << 16, SDIO_RESP_SHORT); // CMD55
//  err = sdio_resp(SD_CMD_APP_CMD, 1, 0);
//  if(err != SDIO_OK) return err;
//  // disable card detect pull-up resistor
//  sdio_cmd(SD_CMD_SET_CLR_CARD_DETECT, 0, SDIO_RESP_SHORT); // ACMD42
//  err = sdio_resp(SD_CMD_SET_CLR_CARD_DETECT, 1, 0);
//  if(err != SDIO_OK) return err;

  if(card.type != CARD_MMC)
  {
    // get SCR
    sdio_read_scr((uint32_t*)card.scr);
    if(card.scr[1] & 0x05) sdio_set_bw4();
  }

  if(card.type == CARD_MMC ||
	 card.type == CARD_SDSC_V1 ||
	 card.type == CARD_SDSC_V2)
  {
    // set block size
    sdio_cmd(SD_CMD_SET_BLOCKLEN, 512, SDIO_RESP_SHORT); // CMD16
    err = sdio_resp(SD_CMD_SET_BLOCKLEN, 1, 0);
    if(err != SDIO_OK) return err;
  }

  return err;
}

uint32_t sdio_read(void *buf, uint32_t addr, uint32_t len)
{
  uint32_t *buf32 = buf;
  uint32_t cmd_res = SDIO_OK;
  uint32_t blk_count = len >> 9; // sectors in block
  uint32_t STA; // to speed up SDIO flags checking

  // init the data ctrl reg
  SDIO->DCTRL = 0;
  if(card.type != CARD_SDHC) addr <<= 9;

  // clear the static SDIO flags
  SDIO->ICR = SDIO_ICR_STATIC;

  uint32_t t = SDIO_STA_DTIMEOUT | SDIO_STA_DCRCFAIL | SDIO_STA_STBITERR | SDIO_STA_RXOVERR;
  if(blk_count > 1)
  {
    t |= SDIO_STA_DATAEND;
    sdio_cmd(SD_CMD_READ_MULT_BLOCK, addr, SDIO_RESP_SHORT); // CMD18
    cmd_res = sdio_resp(SD_CMD_READ_MULT_BLOCK, 1, 0);
  }
  else
  {
    t |= SDIO_STA_DBCKEND;
    sdio_cmd(SD_CMD_READ_SINGLE_BLOCK, addr, SDIO_RESP_SHORT); // CMD17
    cmd_res = sdio_resp(SD_CMD_READ_SINGLE_BLOCK, 1, 0);
  }
  if(cmd_res != SDIO_OK) return cmd_res;

  // data read timeout
  SDIO->DTIMER = SDIO_READ_TIMEOUT;
  // data length
  SDIO->DLEN = len;

  // mode: block
  // dir: to card
  // dma: dis
  // block size: 512 (2^9)
  // DPSM: en
  SDIO->DCTRL = SDIO_DCTRL_DTDIR | SDIO_DCTRL_DTEN | (9 << 4);

  // rcv a data block
  do
  {
    STA = SDIO->STA;
    if(STA & SDIO_STA_RXFIFOHF)
    {
      *buf32++ = SDIO->FIFO;
      *buf32++ = SDIO->FIFO;
      *buf32++ = SDIO->FIFO;
      *buf32++ = SDIO->FIFO;
      *buf32++ = SDIO->FIFO;
      *buf32++ = SDIO->FIFO;
      *buf32++ = SDIO->FIFO;
      *buf32++ = SDIO->FIFO;
    }
  } while(!(STA & t));

  // check for errors
  if(STA & (SDIO_STA_DTIMEOUT | SDIO_STA_DCRCFAIL | SDIO_STA_STBITERR | SDIO_STA_RXOVERR)) return -1;

  // read the data remnant from RX FIFO (if there is still any data)
  while(SDIO->STA & SDIO_STA_RXDAVL) *buf32++ = SDIO->FIFO;

  // clear the static SDIO flags
  SDIO->ICR = SDIO_ICR_STATIC;

  return cmd_res;
}

void sdio_init()
{
  // pin cfg
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN | RCC_AHB1ENR_GPIODEN;
  gpio_init_af_ext(gpio_sdio_init, GPIO_AF_SDIO, GPIO_OTYPE_PP, GPIO_PULLUP);

  // clk enable
  RCC->APB2ENR |= RCC_APB2ENR_SDIOEN;
  // rst
  RCC->APB2RSTR |= RCC_APB2RSTR_SDIORST;
  RCC->APB2RSTR &= ~RCC_APB2RSTR_SDIORST;

  // rising edge
  // buswide: 1-bit
  // pwr saving: dis
  // cld bypass: dis
  // hw flow ctrl: dis
  // clk: en, 400khz
  SDIO->CLKCR = SDIO_CLKCR_CLKEN | SDIO_CLK_DIV_LOW;
  SDIO->POWER = SDIO_POWER_PWRCTRL;
}

