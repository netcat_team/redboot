/*
 * hw_adc.h
 *
 *  Created on: Jun 23, 2020
 *      Author: netcat
 */

#ifndef BSP_HAL_HW_ADC_H_
#define BSP_HAL_HW_ADC_H_

#include "hw_defs.h"

enum
{
  ADC_RESOLUTION_12B = 0,
  ADC_RESOLUTION_10B = ADC_CR1_RES_0,
  ADC_RESOLUTION_8B = ADC_CR1_RES_1,
  ADC_RESOLUTION_6B = ADC_CR1_RES_1 | ADC_CR1_RES_0,
};

void adc_init();
uint32_t adc_val(ADC_TypeDef *ADCx);

#endif /* BSP_HAL_HW_ADC_H_ */
