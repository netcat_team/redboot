/*
 * stm_gpio.h
 *
 *  Created on: Feb 7, 2017
 *      Author: netcat
 */

#ifndef HW_GPIO_H
#define HW_GPIO_H

#include "hw_defs.h"

enum
{
  GPIO_PIN_RESET = 0,
  GPIO_PIN_SET
};

#define gpio_set(gpio, pin) (gpio)->BSRR = (1 << (pin))
#define gpio_reset(gpio, pin) (gpio)->BSRR = ((1 << (pin)) << 16)
#define gpio_toggle(gpio, pin) (gpio)->ODR ^= (1 << (pin))
#define gpio_read(gpio) ((gpio)->IDR)
#define gpio_read_pin(gpio, pin) (((gpio)->IDR & (1 << (pin))) ? GPIO_PIN_SET : GPIO_PIN_RESET)
#define gpio_write_pin(gpio, pin, state) (gpio)->ODR = ((state) ? ((gpio)->ODR | (1 << (pin))) : ((gpio)->ODR & (~(1 << (pin)))))

typedef enum
{
  GPIO_MODE_IN = 0x00,
  GPIO_MODE_OUT = 0x01,
  GPIO_MODE_AF = 0x02,
  GPIO_MODE_AN = 0x03
} gpio_mode_t;

typedef enum
{
  GPIO_OTYPE_PP = 0x00,
  GPIO_OTYPE_OD = 0x01
} gpio_otype_t;

typedef enum
{
  GPIO_NOPULL = 0, // No Pull-up or Pull-down activation
  GPIO_PULLUP = 1, // Pull-up activation
  GPIO_PULLDOWN = 2 // Pull-down activation
} gpio_pupd_t;

typedef enum
{
  GPIO_SPEED_FREQ_LOW = 0, // IO works at 2 MHz
  GPIO_SPEED_FREQ_MEDIUM = 1, // range 12,5 MHz to 50 MHz
  GPIO_SPEED_FREQ_HIGH = 2, // range 25 MHz to 100 MHz
  GPIO_SPEED_FREQ_VERY_HIGH = 3 // range 50 MHz to 200 MHz
} gpio_speed_t;

typedef enum
{
  GPIO_AF_DEFAULT = 0,
  GPIO_AF_RTC_50HZ = 0,
  GPIO_AF_MCO = 0,
  GPIO_AF_TAMPER = 0,
  GPIO_AF_SWJ = 0,
  GPIO_AF_TRACE = 0,
  GPIO_AF_TIM1 = 1,
  GPIO_AF_TIM2 = 1,
  GPIO_AF_TIM3 = 2,
  GPIO_AF_TIM4 = 2,
  GPIO_AF_TIM5 = 2,
  GPIO_AF_TIM8 = 3,
  GPIO_AF_TIM9 = 3,
  GPIO_AF_TIM10 = 3,
  GPIO_AF_TIM11 = 3,
  GPIO_AF_I2C1 = 4,
  GPIO_AF_I2C2 = 4,
  GPIO_AF_I2C3 = 4,
  GPIO_AF_SPI1 = 5,
  GPIO_AF_SPI2 = 5,
  GPIO_AF_SPI4 = 5,
  GPIO_AF_SPI5 = 5,
  GPIO_AF_SPI6 = 5,
  GPIO_AF_SPI3 = 6,
  GPIO_AF_SAI1 = 6,
  GPIO_AF_USART1 = 7,
  GPIO_AF_USART2 = 7,
  GPIO_AF_USART3 = 7,
  GPIO_AF_I2S3ext = 7,
  GPIO_AF_UART4 = 8,
  GPIO_AF_UART5 = 8,
  GPIO_AF_USART6 = 8,
  GPIO_AF_UART7 = 8,
  GPIO_AF_UART8 = 8,
  GPIO_AF_CAN1 = 9,
  GPIO_AF_CAN2 = 9,
  GPIO_AF_TIM12 = 9,
  GPIO_AF_TIM13 = 9,
  GPIO_AF_TIM14 = 9,
  GPIO_AF9_I2C2 = 9,
  GPIO_AF9_I2C3 = 9,
  GPIO_AF_OTG_FS = 10,
  GPIO_AF_OTG_HS = 10,
  GPIO_AF_ETH = 11,
  GPIO_AF_FMC = 12,
  GPIO_AF_OTG_HS_FS = 12,
  GPIO_AF_SDIO = 12,
  GPIO_AF_DCMI = 13,
  GPIO_AF_LTDC = 14,
  GPIO_AF_EVENTOUT = 15,
} gpio_af_t;

typedef struct __attribute__((packed))
{
  GPIO_TypeDef *GPIOx;
  uint32_t pin;
} gpio_init_t;

void gpio_init_ext(GPIO_TypeDef *GPIOx, uint32_t pin, gpio_mode_t mode, gpio_otype_t otype, gpio_speed_t speed, gpio_pupd_t pupd);
void gpio_init_af(const gpio_init_t *array, gpio_af_t af);
void gpio_deinit_af(const gpio_init_t *array);
void gpio_init_af_ext(const gpio_init_t *array, gpio_af_t af, gpio_otype_t otype, gpio_pupd_t pupd);
void gpio_init(GPIO_TypeDef *GPIOx, uint32_t pin, gpio_mode_t mode);
void gpio_set_af(GPIO_TypeDef *GPIOx, uint32_t pin, gpio_af_t af);

#endif
