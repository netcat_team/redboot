/*
 * hw_adc.c
 *
 *  Created on: Jun 23, 2020
 *      Author: netcat
 */

#include "hw_defs.h"
#include "hw_gpio.h"
#include "hal/hw_crc.h"
#include "printf.h"

static void adc_cfg(ADC_TypeDef *ADCx)
{
  ADCx->CR1 = 0;
  ADCx->CR2 = ADC_CR2_ADON;
  ADCx->SMPR2 = ADC_SMPR2_SMP8_2;
  ADCx->CR2 |= ADC_CR2_EXTSEL | ADC_CR2_EXTEN | ADC_CR2_CONT;
  ADCx->SQR1 = 0;
  ADCx->SQR2 = 0;
  ADCx->SQR3 = ADC_SQR3_SQ1_3; // channel 8
  ADCx->CR2 |= ADC_CR2_SWSTART;
  while(!(ADCx->SR & ADC_SR_EOC));
}

uint32_t adc_val(ADC_TypeDef *ADCx)
{
  return ADCx->DR;
}

void adc_init()
{
  // en ADC3 clk
  RCC->APB2ENR |= RCC_APB2ENR_ADC3EN;

  // gpio
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIOFEN;
  gpio_init_ext(GPIOF, 10, GPIO_MODE_AN, GPIO_OTYPE_PP, GPIO_SPEED_FREQ_LOW, GPIO_NOPULL);

  adc_cfg(ADC3);
}
