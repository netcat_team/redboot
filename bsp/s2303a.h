/*
 * s2303a.h
 *
 *  Created on: Feb 1, 2016
 *      Author: netcat
 */

#ifndef BSP_S2303A_H_
#define BSP_S2303A_H_

#include <stdint.h>

#define S2303A_ADDR 0x20
#define S2303A_PRODUCT_ID 0x93
#define S2303A_REG_ST 0x15
#define S2303A_REG_POS 0x18

typedef struct __attribute__((packed))
{
  uint8_t x1;
  uint8_t y1;
  union
  {
    uint8_t y0 : 4;
    uint8_t x0 : 4;
  };
  union
  {
    uint8_t wx : 4;
    uint8_t wy : 4;
  };
  uint8_t z;
} ts_pos_t;

typedef struct
{
  uint16_t x;
  uint16_t y;
  uint8_t st;
} ts_t;

enum
{
  TS_ST_NO_TOUCH = 0,
  TS_ST_TOUCH,
};

uint32_t s3203_read(uint8_t reg, void *buf, uint32_t cnt);
ts_t s3203_ts();

#endif /* BSP_S2303A_H_ */
