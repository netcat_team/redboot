/*
 * tps65217.c
 *
 *  Created on: Dec 27, 2018
 *      Author: netcat
 */
#include <stdint.h>

#include "tps65217.h"
#include "hal/hw_i2c.h"
#include "bsp.h"
#include "printf.h"

#include "sys/cli/syslog.h"

const uint8_t tps65217_lvl_table[] = {
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 2,
  2, 2, 2, 2, 2, 2, 1, 0, 1, 1, 1, 1, 1, 1, 1 };

static uint32_t pwr_reg_write(uint8_t reg, uint8_t val)
{
  uint32_t ret = 0;
  uint8_t addr = I2C_DEV_TPS65217;
  uint8_t reg_pwd = TPS65217_PASSWORD;
  uint8_t pwd = reg ^ 0x7D;
  uint32_t lvl = tps65217_lvl_table[reg];

  const iovec v_pwd[] = {
      { &addr, sizeof(addr) },
      { &reg_pwd, sizeof(reg_pwd) },
      { &pwd, sizeof(pwd) },
  };

  const iovec v_data[] = {
      { &addr, sizeof(addr) },
      { &reg, sizeof(reg) },
      { &val, sizeof(val) },
  };

  switch(lvl)
  {
    case 2:
    {
      ret = i2c_io(I2C1, v_pwd, SIZE_OF_ARRAY(v_pwd), 0, 0);
      if(ret) return ret;
      ret = i2c_io(I2C1, v_data, SIZE_OF_ARRAY(v_data), 0, 0);
      if(ret) return ret;
      ret = i2c_io(I2C1, v_pwd, SIZE_OF_ARRAY(v_pwd), 0, 0);
      if(ret) return ret;
      ret = i2c_io(I2C1, v_data, SIZE_OF_ARRAY(v_data), 0, 0);
    }
    break;

    case 1:
    {
      ret = i2c_io(I2C1, v_pwd, SIZE_OF_ARRAY(v_pwd), 0, 0);
      if(ret) return ret;
      ret = i2c_io(I2C1, v_data, SIZE_OF_ARRAY(v_data), 0, 0);
    }
    break;

    default:
    {
      ret = i2c_io(I2C1, v_data, SIZE_OF_ARRAY(v_data), 0, 0);
    }
    break;
  }

  return ret;
}

uint8_t pwr_id(uint8_t *id)
{
  uint8_t addr = I2C_DEV_TPS65217;
  uint8_t reg = TPS65217_CHIPID;
  const iovec v[] = {
      { &addr, sizeof(addr) },
      { &reg, sizeof(reg) },
  };
  return i2c_io(I2C1, v, SIZE_OF_ARRAY(v), id, sizeof(uint8_t));
}

uint8_t pwr_charging_status(uint8_t *st)
{
  uint8_t addr = I2C_DEV_TPS65217;
  uint8_t reg = TPS65217_CHGCONFIG0;
  const iovec v[] = {
      { &addr, sizeof(addr) },
      { &reg, sizeof(reg) },
  };
  return i2c_io(I2C1, v, SIZE_OF_ARRAY(v), st, sizeof(uint8_t));
}

const pwr_conf_t pwr_conf[] = {

  // current conf
  { TPS65217_PPATH, 0x2f }, // USB = 1800mA

  // LDO conf
  { TPS65217_DEFLS1, 0x2f }, // set LDO3 = 2.5v
  { TPS65217_DEFLS2, 0x20 }, // set LDO2 = 1.5v
  { TPS65217_DEFLDO2, 0x38 }, // set LDO2 = 3.3v
  { TPS65217_DEFLDO1, 0x0f }, // set LDO1 = 3.3v

  // setup dc-dc values
  // LDO2 - blocking i2c line if disabled
  // DC3 - control pull-up resistor on i2c line (pcb_v2.0 issue)
  // DC1 - MCU power line
  { TPS65217_ENABLE, BSP_PWR_CONF_DEFAULT },
  { TPS65217_DEFDCDC1, 0x37 }, // set DC1 = 3.3v
  { TPS65217_DEFDCDC2, 0x3f }, // set DC2 = ADJ
  { TPS65217_DEFDCDC3, 0x38 }, // set DC3 = 3.3v
  { TPS65217_DEFSLEW, 0x86 }, // apply config of DC-DC (GO)

  // set up sequencer
  { TPS65217_SEQ1, 0x10 }, // dis DC2, en DC1 on strobe 1
  { TPS65217_SEQ2, 0x10 }, // dis LDO1, en DC3 on strobe 1
  { TPS65217_SEQ3, 0x00 }, // dis LDO2 & LDO3
  { TPS65217_SEQ4, 0x00 }, // dis LDO4

  // mux
  { TPS65217_MUXCTRL, 0x01 }, // dis LDO4
};

static void pwr_apply_conf(const pwr_conf_t *conf, uint32_t size)
{
  const pwr_conf_t *end = conf + size;
  while(conf++ < end) pwr_reg_write(conf->reg, conf->val);
}

void pwr_init()
{
  pwr_apply_conf(pwr_conf, SIZE_OF_ARRAY(pwr_conf));
//  printf("set backlight %d", BSP_DEFAULT_BL);
  pwr_backlight(BSP_DEFAULT_BL);
}

void pwr_shutdown()
{
  pwr_reg_write(TPS65217_SEQ6, 0x03); // instant shutdown
}

void pwr_ctrl(uint32_t modem, uint32_t fpga, uint32_t audio)
{
  uint32_t cfg = BSP_LDO2_VSENS | BSP_DCDC3_3V3 | BSP_DCDC1_VMCU;

  if(modem) cfg |= BSP_DCDC2_4V;
  if(fpga) cfg |= BSP_LDO3_2V5 | BSP_LDO4_1V2;
  if(audio) cfg |= BSP_LDO1_VSND;

  syslog(SYSLOG_DEBUG, "PWR: modem = %d, FPGA = %d, audio = %d", modem, fpga, audio);

  pwr_reg_write(TPS65217_ENABLE, cfg);
}

void pwr_backlight(uint32_t value)
{
  if(value)
  {
    pwr_reg_write(TPS65217_WLEDCTRL1, 0x08 | 0x03);
    pwr_reg_write(TPS65217_WLEDCTRL2, value);
  }
  else
  {
    pwr_reg_write(TPS65217_WLEDCTRL1, 0x01);
    pwr_reg_write(TPS65217_WLEDCTRL2, 0x00);
  }
}

