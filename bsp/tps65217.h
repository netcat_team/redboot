/*
 * tps65217.h
 *
 *  Created on: Dec 27, 2018
 *      Author: netcat
 */

#ifndef TPS65217_H_
#define TPS65217_H_

#define TPS65217_CHIP_ID 0xf0
#define TPS65217_CHIP_ID_B 0xf0

enum
{
  TPS65217_CHIPID = 0,
  TPS65217_PPATH,
  TPS65217_INT,
  TPS65217_CHGCONFIG0,
  TPS65217_CHGCONFIG1,
  TPS65217_CHGCONFIG2,
  TPS65217_CHGCONFIG3,
  TPS65217_WLEDCTRL1,
  TPS65217_WLEDCTRL2,
  TPS65217_MUXCTRL,
  TPS65217_STATUS,
  TPS65217_PASSWORD,
  TPS65217_PGOOD,
  TPS65217_DEFPG,
  TPS65217_DEFDCDC1,
  TPS65217_DEFDCDC2,
  TPS65217_DEFDCDC3,
  TPS65217_DEFSLEW,
  TPS65217_DEFLDO1,
  TPS65217_DEFLDO2,
  TPS65217_DEFLS1,
  TPS65217_DEFLS2,
  TPS65217_ENABLE,
  TPS65217_UNKNOWN,
  TPS65217_DEFUVLO,
  TPS65217_SEQ1,
  TPS65217_SEQ2,
  TPS65217_SEQ3,
  TPS65217_SEQ4,
  TPS65217_SEQ5,
  TPS65217_SEQ6,
};

typedef struct __attribute__ ((packed))
{
  uint8_t reg;
  uint8_t val;
} pwr_conf_t;

void pwr_init(void);
uint8_t pwr_id(uint8_t *id);
uint8_t pwr_charging_status(uint8_t *st);
void pwr_shutdown(void);
void pwr_ctrl(uint32_t modem, uint32_t fpga, uint32_t audio);
void pwr_backlight(uint32_t value);

#endif /* TPS65217_H_ */
