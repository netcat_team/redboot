/*
 * w25q64.c
 *
 *  Created on: Apr 6, 2019
 *      Author: vchumakov
 */

#include "hw_spi.h"
#include "hw_gpio.h"
#include "w25q64.h"
#include "syslog.h"
#include "bsp.h"

static void spimem_sel(uint32_t select)
{
  if(select) gpio_set(EPCS_CS_PORT, EPCS_CS_PIN);
  else gpio_reset(EPCS_CS_PORT, EPCS_CS_PIN);
}

spi_dev spimem_spi = { SPI1, spimem_sel, 0 };

uint8_t spimem_id()
{
  uint8_t cmd = SPIMEM_CMD_READ_ID;
  uint8_t st[5];
  iovec v = { &cmd, sizeof(cmd) };
  spi_io(&spimem_spi, &v, 1, &st, sizeof(st));

  return st[0];
}

uint32_t spimem_st()
{
  uint8_t cmd = SPIMEM_CMD_READ_ST;
  uint8_t st = 0;
  const iovec v = { &cmd, sizeof(cmd) };
  spi_io(&spimem_spi, &v, 1, &st, sizeof(st));

//  syslog(SYSLOG_DEBUG, "SR = %02x", st);

  return st;
}

uint32_t spimem_busy(volatile uint32_t ms)
{
  while(ms--)
  {
    wait_ms(1);
    if(!(spimem_st() & (1 << SPIMEM_SR_RDY))) return 0;
  }
  return 1;
}

uint32_t spimem_we()
{
  uint8_t cmd = SPIMEM_CMD_WE_EN;
  const iovec v[] = { { (uint8_t *)&cmd, sizeof(cmd) } };
  spi_io(&spimem_spi, v, SIZE_OF_ARRAY(v), 0, 0);

  return !(spimem_st() & (1 << SPIMEM_SR_WEL));
}

void spimem_erase_sector(uint32_t sector)
{
//  syslog(SYSLOG_DEBUG, "erase sector = %08x", sector);

  if(spimem_we())
  {
     syslog(SYSLOG_ERR, "SPIMEM no WEN");
     return;
  }

  uint8_t cmd = SPIMEM_CMD_ERASE_SECTOR;
  uint8_t addr[] = { sector >> 16, sector >> 8, sector };
  const iovec v[] = {
      { &cmd, sizeof(cmd) },
      { addr, sizeof(addr) }
  };
  spi_io(&spimem_spi, v, SIZE_OF_ARRAY(v), 0, 0);

  if(spimem_busy(W25Q64_PAGE_ERASE_MS))
  {
    syslog(SYSLOG_ERR, "SPIMEM busy");
  }
}

uint32_t spimem_write(uint32_t addr, uint8_t *data, uint32_t count)
{
  while(count)
  {
    uint32_t wrb = MIN(count, W25Q64_PAGE_SIZE - (addr % W25Q64_PAGE_SIZE));

    if(!(addr % W25Q64_SECTOR_SIZE))
    {
      spimem_erase_sector(addr);
    }

    if(spimem_we())
    {
       syslog(SYSLOG_ERR, "SPIMEM no WEN");
       return 1;
    }

    uint8_t cmd = SPIMEM_CMD_WRITE;
    uint8_t addr24[] = { addr >> 16, addr >> 8, addr };
    const iovec v[] = {
        { &cmd, sizeof(cmd) },
        { &addr24, sizeof(addr24) },
        { data, wrb },
    };

    spi_io(&spimem_spi, v, SIZE_OF_ARRAY(v), 0, 0);

    if(spimem_busy(W25Q64_PAGE_WRITE_MS))
    {
      syslog(SYSLOG_ERR, "SPIMEM busy");
      return 1;
    }

    addr += wrb;
    data += wrb;
    count -= wrb;
  }

  return 0;
}

uint32_t spimem_read(uint32_t addr, uint8_t *buf, uint32_t size)
{
  uint8_t cmd = SPIMEM_CMD_READ;
  uint8_t addr24[] = { addr >> 16, addr >> 8, addr };
  const iovec v[] = {
      { &cmd, sizeof(cmd) },
      { &addr24, sizeof(addr24) }
  };
  spi_io(&spimem_spi, v, SIZE_OF_ARRAY(v), buf, size);

  return 0;
}

