/*
 * rm68120.h
 *
 *  Created on: Apr 9, 2017
 *      Author: netcat
 */

#ifndef RM68120_H_
#define RM68120_H_

#define LCD_ADDR_REG ((uint32_t)0x60000000)
#define LCD_ADDR_DATA ((uint32_t)0x60004000)
#define LCD_WRITE_REG(addr) (*(volatile uint16_t*)LCD_ADDR_REG) = addr
#define LCD_WRITE_DATA(data) (*(volatile uint16_t*)LCD_ADDR_DATA) = data
#define LCD_READ_DATA() (*(uint16_t*)(0x60004000))
#define LCD_ADDR_REG ((uint32_t)0x60000000)

#define LCD_MAXX 480
#define LCD_MAXY 800
#define LCD_BPP 2
#define LCD_FRAME_BUFFER_SIZE ((uint32_t)(LCD_MAXX * LCD_MAXY * LCD_BPP))

void lcd_init();
void lcd_set_addr0();

#endif /* RM68120_H_ */
