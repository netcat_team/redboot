/*
 * memmap.h
 *
 *  Created on: May 21, 2020
 *      Author: netcat
 */

#ifndef BSP_MEMMAP_H_
#define BSP_MEMMAP_H_

#include "hw_defs.h"
#include "hw_fmc.h"

// APP region
#define MEM_APP_ADDR SDRAM_ADDR
#define MEM_APP_SIZE MB(8)
// heap region
#define MEM_HEAP_ADDR (MEM_APP_ADDR + MEM_APP_SIZE)
#define MEM_HEAP_SIZE MB(20)
// GMA region
#define MEM_GMA_ADDR (MEM_HEAP_ADDR + MEM_HEAP_SIZE)
#define MEM_GMA_SIZE MB(4)

// LCD config
#define LCD_WIDTH 480
#define LCD_HEIGHT 800
#define LCD_BPP 2
#define LCD_SIZE (LCD_WIDTH * LCD_HEIGHT)
#define LCD_FB_SIZE (LCD_SIZE * LCD_BPP)
#define LCD_FB_ADDR MEM_GMA_ADDR
#define LCD_FB_PTR ((void *)(LCD_FB_ADDR))
#define LCD_CLUT_ADDR (LCD_FB_ADDR + LCD_FB_SIZE)
#define LCD_CLUT_SIZE LCD_SIZE

#endif /* BSP_MEMMAP_H_ */
