#include "hal/hw_gpio.h"
#include "hal/hw_rcc.h"

#include "sys/cli/vt.h"
#include "sys/cli/apps.h"
#include "bsp/bsp.h"
#include "bsp/tps65217.h"
#include "hw_ltdc.h"

#include "sys/cli/printf.h"
#include "sys/cli/syslog.h"

#include "vfs.h"
#include "sys/rbuf.h"
#include "ota_ctl.h"

void wdt_reset();

void HardFault_Handler()
{
  int t = 10;
  while(t--)
  {
    gpio_toggle(LEDS_PORT, LED_YELLOW_PIN);
    wait_ms(50);
  }
  wdt_reset();
}

rbuf_t ctrl_rbuf;

void USART3_IRQHandler(void)
{
  if(USART3->SR & USART_SR_RXNE)
  {
    uint8_t data = USART3->DR;
    USART3->SR &= ~USART_SR_RXNE;
    rbuf_put(&ctrl_rbuf, data);
  }
}

rbuf_t vt_rbuf;

void USART1_IRQHandler(void)
{
  if(USART1->SR & USART_SR_RXNE)
  {
    USART1->SR &= ~USART_SR_RXNE;
    rbuf_put(&vt_rbuf, USART1->DR);
  }
}

int main()
{
  bsp_sys_init();
  apps_init();

  // cli
  rbuf_init(&vt_rbuf, 128);
  vt_t vt_uart;
  vt_init(&vt_uart, apps_run, (vt_fp)bsp_cli_str, 0, " > ");
  NVIC_EnableIRQ(USART1_IRQn);
  NVIC_SetPriority(USART1_IRQn, 14);

  // ota
  rbuf_init(&ctrl_rbuf, OTA_BUF_SIZE);
  NVIC_EnableIRQ(USART3_IRQn);
  NVIC_SetPriority(USART3_IRQn, 14);
  ota_event(CMD_ACC_WAKEUP);

  // ctrl
  NVIC_SetPriority(USART6_IRQn, 14);

  while(1)
  {
    // test i2c bus
    uint8_t id;
    if(!pwr_id(&id))
    {
      if(id == TPS65217_CHIP_ID_B) gpio_toggle(LEDS_PORT, LED_GREEN_PIN);
      else gpio_reset(LEDS_PORT, LED_GREEN_PIN);
    }
//    if(gpio_read_pin(FPGA_CONF_DONE_PORT, FPGA_CONF_DONE_PIN)) gpio_set(LEDS_PORT, LED_YELLOW_PIN);
//    else gpio_reset(LEDS_PORT, LED_YELLOW_PIN);

    uint8_t ret, rx;

    ret = rbuf_get(&vt_rbuf, &rx);
    if(ret) vt_input(&vt_uart, rx);

    ret = rbuf_get(&ctrl_rbuf, &rx);
    if(ret) ota_rcv(rx);
  }

	return 0;
}
