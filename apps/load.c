/*
 * load.c
 *
 *  Created on: May 3, 2020
 *      Author: netcat
 */

#include "printf.h"
#include "apps.h"

#include "bsp.h"
#include "vfs.h"
#include "getopt.h"

SYS_APP(load);

#define LOAD_BUF_SIZE 4096

void cp_file(uint32_t addr, const char *str, const char *f_name)
{
  FILE *f = fopen(f_name, "rb");
  int ld = 0;

  if(f)
  {
    fseek(f, 0, SEEK_END);
    uint32_t size = ftell(f);
    fseek(f, 0, SEEK_SET);

    if(size > 100000) ld = true;

    printf("loading %s to 0x%08lx, size: %ld bytes \n", str, addr, size);

    uint8_t *buf = malloc(LOAD_BUF_SIZE);
    uint8_t *mem = (uint8_t *)addr;

    uint32_t load_st = 0;
    while(size)
    {
      uint32_t wrb = MIN(size, LOAD_BUF_SIZE);
      fread(buf, 1, wrb, f);
      memcpy(mem, buf, wrb);

      mem += wrb;
      size -= wrb;

      if(ld && !(load_st++ % 16)) printf(".");
    }
    if(ld) printf(" ok \n");

    free(buf);
    fclose(f);
  }
  else
  {
    printf("file %s not found \n", f_name);
  }
}

int load_main(int argc, char **argv)
{
  getopt_ctx_t ctx;
  getopt_init(&ctx);

  uint32_t addr = 0;
  char *fname = 0;
  char *label = 0;

  while(getopt(&ctx, argc, argv, "a:f:l:"))
  {
    switch(ctx.err)
    {
      case 'a': addr = htoi(ctx.arg); break;
      case 'f': fname = ctx.arg; break;
      case 'l': label = ctx.arg; break;

      default:
        break;
    }
  }

  if(fname && addr)
  {
    cp_file(addr, label ? label : fname, fname);
  }

  return 0;
}


