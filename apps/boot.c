/*
 * load.c
 *
 *  Created on: Aug 6, 2019
 *      Author: netcat
 */

#include "apps.h"
#include "printf.h"
#include "syslog.h"
#include "getopt.h"
#include "bsp.h"
#include "memmap.h"

SYS_APP(boot);

static void (*xip)(uint32_t res, uint32_t mach, uint32_t dtb);

int boot_main(int argc, char **argv)
{
  getopt_ctx_t ctx;
  getopt_init(&ctx);

  uint32_t addr = MEM_APP_ADDR;
  uint32_t linux_dbt_addr = 0;

  while(getopt(&ctx, argc, argv, "a:l:rtf"))
  {
    switch(ctx.err)
    {
      case 'a': addr = htoi(ctx.arg); break;
      case 'l': linux_dbt_addr = htoi(ctx.arg); break;

      default:
        break;
    }
  }

  if(addr)
  {
    if(linux_dbt_addr)
    {
      printf("Starting kernel... \n");
      xip = (void (*)(uint32_t, uint32_t, uint32_t))(addr + 1);
      xip(0, 0xffffffff, linux_dbt_addr);
    }
    else
    {
      printf("Starting firmware... \n");
      SCB->VTOR = addr;
      ((void(*)(void))addr + 1)();
    }
  }

  return 0;
}


