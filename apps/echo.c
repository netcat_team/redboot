/*
 * echo.c
 *
 *  Created on: May 3, 2020
 *      Author: netcat
 */

#include "printf.h"
#include "apps.h"
#include "bsp.h"

SYS_APP(echo);

int echo_main(int argc, char **argv)
{
  for(int i = 1; i < argc; i++) printf("%s ", argv[i]);
  printf("\n");

  return 0;
}
