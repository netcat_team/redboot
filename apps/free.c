/*
 * free.c
 *
 *  Created on: Jul 2, 2019
 *      Author: netcat
 */

#include "sys/cli/printf.h"
#include "sys/cli/apps.h"
#include "sys/cli/vt.h"
#include "sys/heap.h"
#include "memmap.h"

SYS_APP(free);

int free_main(int argc, char **argv)
{
  printf("%10s  %10s %10s %10s %10s \n",
    "", "offset", "used", "total", "free");

  // sdram
  printf("%10s: %10x %10u %10x %10u \n", "sdram", (int)SDRAM_ADDR, SDRAM_SIZE, SDRAM_SIZE, 0);

  // app
  printf("%10s: %10x %10u %10x %10u \n", "app", (int)MEM_APP_ADDR, 0, MEM_APP_SIZE, 0);

  // heap
  extern heap_header_t sys_heap;
  printf("%10s: %10x %10u %10x %10u \n", "heap",
    (int)sys_heap.addr, (int)(sys_heap.size - sys_heap.remaining),
    (int)sys_heap.size, (int)sys_heap.remaining);

  // gma
  printf("%10s: %10x %10u %10x %10u \n", "gma", (int)MEM_GMA_ADDR, 0, MEM_GMA_SIZE, 0);
  // fb
  printf("%10s: %10x %10u %10x %10u \n", "fb", (int)LCD_FB_ADDR, LCD_FB_SIZE, LCD_FB_SIZE, 0);
  // clut
  printf("%10s: %10x %10u %10x %10u \n", "clut", (int)LCD_CLUT_ADDR, LCD_CLUT_SIZE, LCD_CLUT_SIZE, 0);

  return 0;
}



