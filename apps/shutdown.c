/*
 * ls.c
 *
 *  Created on: Jul 9, 2019
 *      Author: netcat
 */

#include "sys/cli/printf.h"
#include "sys/cli/apps.h"
#include "bsp/bsp.h"
#include "bsp/tps65217.h"
#include "hal/hw_gpio.h"

SYS_APP(shutdown);

int shutdown_main(int argc, char **argv)
{
  printf("System going down \n");
//  gpio_reset(PWR_ENABLE_PORT, PWR_ENABLE_PIN);
  pwr_backlight(0);
  pwr_shutdown();
  return 0;
}
