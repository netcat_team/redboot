/*
 * cat.c
 *
 *  Created on: Jul 2, 2019
 *      Author: netcat
 */

#include "sys/cli/printf.h"
#include "sys/cli/apps.h"

#include "bsp/bsp.h"
#include "vfs.h"

SYS_APP(cat);

int cat_main(int argc, char **argv)
{
  int i;
  for(i = 1; i < argc; i++)
  {
    FILE *f = fopen(argv[i], "rb");
    if(f)
    {
      fseek(f, 0, SEEK_END);
      uint32_t size = ftell(f);
      fseek(f, 0, SEEK_SET);

      uint8_t *buf = malloc(size + 1);
      if(buf)
      {
        size = fread(buf, 1, size, f);
        buf[size] = 0;

        printf("\"%s\":\n%s\n", argv[i], buf);
        free(buf);
      }
      fclose(f);
    }
  }
  return 0;
}


