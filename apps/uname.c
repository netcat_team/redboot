/*
 * uname.c
 *
 *  Created on: May 7, 2020
 *      Author: netcat
 */

#include "printf.h"
#include "apps.h"
#include "bsp.h"

SYS_APP(uname);

int uname_main(int argc, char **argv)
{
  printf("%s\n", BSP_UNAME);
  return 0;
}
