/*
 * flash.c
 *
 *  Created on: Jul 1, 2019
 *      Author: netcat
 */

#include "sys/cli/printf.h"
#include "sys/cli/apps.h"

#include "bsp/bsp.h"
#include "bsp/w25q64.h"
#include "hal/hw_gpio.h"
#include "vfs.h"

#include "sys/cli/syslog.h"

SYS_APP(flash);

uint8_t bit_rev(uint8_t b)
{
  b = (b & 0xF0) >> 4 | (b & 0x0F) << 4;
  b = (b & 0xCC) >> 2 | (b & 0x33) << 2;
  b = (b & 0xAA) >> 1 | (b & 0x55) << 1;
  return b;
}

void flash_write_file(const char *path)
{
  if(!path) return;

  FILE *f = fopen(path, "rb");
  if(f)
  {
    fseek(f, 0, SEEK_END);
    uint32_t size = ftell(f);
    fseek(f, 0, SEEK_SET);

    uint32_t addr = 0;
    uint8_t buf[W25Q64_PAGE_SIZE];

    while(size)
    {
      uint32_t wb = MIN(size, W25Q64_PAGE_SIZE);

      fread(buf, 1, wb, f);
      for(int i = 0; i < wb; i++) buf[i] = bit_rev(buf[i]);

      spimem_write(addr, buf, wb);

      size -= wb;
      addr += wb;
      if(!(size % 0x10000)) printf(".");
    }
    fclose(f);
    printf("\n");

    syslog(SYSLOG_DEBUG, "program done!");
  }
}

void flash_verify_file(const char *path)
{
  if(!path) return;

  FILE *f = fopen(path, "rb");
  if(f)
  {
    fseek(f, 0, SEEK_END);
    uint32_t size = ftell(f);
    fseek(f, 0, SEEK_SET);

    uint32_t addr = 0;
    uint8_t fbuf[W25Q64_PAGE_SIZE];
    uint8_t mbuf[W25Q64_PAGE_SIZE];

    while(size)
    {
      uint32_t wb = MIN(size, W25Q64_PAGE_SIZE);

      fread(fbuf, 1, wb, f);
      spimem_read(addr, mbuf, wb);

      for(uint32_t i = 0; i < wb; i++)
      {
        if(fbuf[i] != bit_rev(mbuf[i]))
        {
          syslog(SYSLOG_ERR, "[%04x]: FILE = %02x, MEM = %02x", addr + i, fbuf[i], mbuf[i]);
          if(addr > 0x1000 ) return ;
        }
      }

      size -= wb;
      addr += wb;
      if(!(size % 0x10000)) printf(".");
    }
    fclose(f);
    printf("\n");

    syslog(SYSLOG_DEBUG, "verify done!");
  }
}

int flash_main(int argc, char **argv)
{
  flash_write_file(argv[1]);
  flash_verify_file(argv[1]);

  spi1_deinit();
  gpio_init(EPCS_CS_PORT, EPCS_CS_PIN, GPIO_MODE_IN);
  pwr_ctrl(1, 1, 1);

	return 0;
}
