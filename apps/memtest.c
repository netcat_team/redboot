/*
 * memtest.c
 *
 *  Created on: Jul 23, 2019
 *      Author: netcat
 */

#include "hal/hw_fmc.h"
#include "sys/cli/apps.h"
#include "sys/cli/printf.h"
#include "sys/cli/syslog.h"

SYS_APP(memtest);

int memtest_main(int argc, char **argv)
{
  uint32_t pattern[] = {
      0x11111111, 0x55AA55AA, 0x22222222, 0xAA55AA55, 0x44444444,
      0xFFFFFFFF, 0x88888888, 0x77777777, 0xEEEEEEEE, 0x00000000,
      0x0F0F0F0F, 0xF0F0F0F0, 0x12345678, 0x87654321, 0x12481248 };

  volatile uint32_t *sdram = (uint32_t *)SDRAM_ADDR;

  for(uint32_t addr = 0; addr < SDRAM_SIZE / sizeof(uint32_t); addr++)
  {
    if(!(addr & (1024 * 256 - 1))) printf(".");

    uint32_t backup = sdram[addr];
    for(uint32_t j = 0; j < SIZE_OF_ARRAY(pattern); j++)
    {
      sdram[addr] = pattern[j];
      if(sdram[addr] != pattern[j])
      {
        printf("\n");
        syslog(SYSLOG_ERR, "[0x%08x] != 0x%08x", addr, pattern[j]);
        return 0;
      }
    }
    sdram[addr] = backup;
  }

  printf("\n");
  syslog(SYSLOG_DEBUG, "test passed");
  return 0;
}


