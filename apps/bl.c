/*
 * ls.c
 *
 *  Created on: Jul 9, 2019
 *      Author: netcat
 */

#include "sys/cli/printf.h"
#include "sys/cli/apps.h"
#include "bsp/bsp.h"
#include "bsp/tps65217.h"
#include "hal/hw_gpio.h"
#include "hal/hw_tim.h"

SYS_APP(bl);

int bl_main(int argc, char **argv)
{
  if(argc == 2)
  {
    uint32_t val = atoi(argv[1]);
    if(val > 99) val = 99;
    pwr_backlight(val);
  }

  return 0;
}
