/*
 * date.c
 *
 *  Created on: Jul 23, 2019
 *      Author: netcat
 */

#include "hal/hw_rtc.h"
#include "sys/cli/apps.h"
#include "sys/cli/printf.h"

SYS_APP(date);

int date_main(int argc, char **argv)
{
  rtc_dt_t dt;
  rtc_dt(&dt);

  const char *wd_str[] = { "Mo", "Tu", "We", "Th", "Fr", "Sa", "Su" };

  printf("%s %02d/%02d/%d %02d:%02d:%02d \n",
      wd_str[dt.date.wd - 1], dt.date.day, dt.date.month, 2000 + dt.date.year,
      dt.time.hour, dt.time.min, dt.time.sec);

  return 0;
}





