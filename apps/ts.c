/*
 * ts.c
 *
 *  Created on: May 31, 2020
 *      Author: netcat
 */

#include "printf.h"
#include "apps.h"

#include "bsp.h"
#include "hw_gpio.h"
#include "hw_ltdc.h"
#include "hw_dma2d.h"

#include "gma.h"
#include "s2303a.h"

SYS_APP(ts);

#define CNT 4

uint32_t s3203_write(uint8_t reg, uint8_t val)
{
  uint8_t dev = S2303A_ADDR;
  const iovec v[] = { { &dev, sizeof(dev) }, { &reg, sizeof(reg) }, { &val, sizeof(val) }};
  return i2c_io(I2C3, v, SIZE_OF_ARRAY(v), 0, 0);
}

int ts_main(int argc, char **argv)
{
  uint16_t lx[CNT] = {}, ly[CNT] = {};
  uint32_t clr[] = { RGB(0, 255, 255), RGB(255, 0, 255), RGB(255, 0, 255), RGB(0, 0, 255) };

  s3203_write(0x5c, LCD_WIDTH & 0xff); // max x
  s3203_write(0x5d, LCD_WIDTH >> 8);
  s3203_write(0x5e, LCD_HEIGHT & 0xff); // max y
  s3203_write(0x5f, LCD_HEIGHT >> 8);

  while(1)
  {
    if(!gpio_read_pin(PWR_PB_PORT, PWR_PB_PIN))
    {
      printf("break \n");
      break;
    }

    uint8_t st;
    s3203_read(S2303A_REG_ST, &st, sizeof(st));

    for(int i = 0; i < CNT; i++)
    {
      ts_pos_t pos;
      s3203_read(S2303A_REG_POS + i * sizeof(ts_pos_t), &pos, sizeof(pos));

      uint16_t x = (pos.x1 << 4) | pos.x0; // 0 .. 940
      uint16_t y = (pos.y1 << 4) | pos.y0; // 0 .. 1900

      if(st & (1 << (i * 2)))
      {
        if(lx[i] != x || ly[i] != y)
        {
          gma_set_color(RGB(0, 0, 0));
          gma_fill_rect_dma_clut(lx[i], ly[i], 50, 50);

          lx[i] = x;
          ly[i] = y;

          gma_set_color(clr[i]);
          gma_fill_rect_dma_clut(x, y, 50, 50);

          printf("x: %3d, y: %3d \n", x, y);
        }
      }
      else
      {
        gma_set_color(RGB(0, 0, 0));
        gma_fill_rect_dma_clut(lx[i], ly[i], 50, 50);
      }
    }
  }

  return 0;
}





