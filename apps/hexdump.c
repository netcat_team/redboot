/*
 * hexdump.c
 *
 *  Created on: Jul 22, 2019
 *      Author: netcat
 */

#include "printf.h"
#include "apps.h"

#include "bsp.h"
#include "vfs.h"

SYS_APP(hexdump);

void printf_mem(const void *ptr, int addr_offset, int addr_size,
    int item_per_line, int item_size, int count,
    int show_addr, int hide_null)
{
  int i;
  int row_cnt = 0;
  const char *s[] = { "%02x", "%04x", "", "%08x", "--", " -- ", "", "   --   " };

  for(i = 0; i < count; i += item_size)
  {
    uint32_t t;
    if(item_size == 1) t = *(uint8_t *)ptr;
    else if(item_size == 2) t = *(uint16_t *)ptr;
    else t = *(uint32_t *)ptr;

    if(show_addr && (!row_cnt))
    {
      printf(s[addr_size - 1], i + addr_offset);
      printf(": ");
    }

    row_cnt++;
    ptr += item_size;

    if(!t && hide_null) printf(s[item_size + 4 - 1]);
    else printf(s[item_size - 1], t);

    if(row_cnt >= item_per_line)
    {
      row_cnt = 0;
      printf("\n");
    }
    else printf(" ");

  }
  // end if row not fit
  if(row_cnt) printf("\n");
}

int hexdump_main(int argc, char **argv)
{
//  getopt_ctx_t ctx;
//  getopt_init(&ctx);

  int i;
  for(i = 1; i < argc; i++)
  {
    FILE *f = fopen(argv[i], "rb");
    int size = 128;
    if(f)
    {
      uint8_t *buf = malloc(size);
      if(buf)
      {
        size = fread(buf, 1, size, f);
        printf_mem(buf, 0, 4, 8, 2, size, 1, 0);
        free(buf);
      }
      else
      {
        printf("cannot allocate %d bytes\n", size);
      }
      fclose(f);
    }
  }
  return 0;
}



