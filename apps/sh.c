/*
 * sh.c
 *
 *  Created on: May 1, 2020
 *      Author: netcat
 */

#include "printf.h"
#include "apps.h"

#include "bsp.h"
#include "vfs.h"

SYS_APP(sh);

int sh_file_cmdline(FILE *f, char *buf, int size)
{
  int cnt = 0;
  int sf = 0;

  while(1)
  {
    int ret = fgetc(f);

    if(!cnt && ret == EOF) return EOF;
    else if(ret == EOF || ret == '\n')
    {
      sf = 0;
      if(cnt >= size) cnt = 0; // return nothing if over size
      buf[cnt] = 0;

      // for 'exit' cmd - return EOF
      if(cnt >= 4 && !strncmp(buf, "exit", 4)) return EOF;

      return cnt;
    }
    else if(!sf && cnt < size)
    {
      if(ret == '#')
      {
        sf = 1;
        buf[cnt] = 0;
        continue;
      }
      buf[cnt] = ret;
      cnt++;
    }
  }
  return 0;
}

int sh_main(int argc, char **argv)
{
  FILE *f = fopen(argv[1], "rb");
  if(f)
  {
    int len = 0;
    char buf[128];

    while(len != EOF)
    {
      len = sh_file_cmdline(f, buf, sizeof(buf));
      if(len > 0)
      {
//        printf("%s \n", buf);
        apps_run(buf);
      }
    }
    fclose(f);
  }
  return 0;
}





