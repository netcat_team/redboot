/*
 * gms.c
 *
 *  Created on: Aug 26, 2019
 *      Author: netcat
 */

#include "apps.h"
#include "bsp.h"
#include "hw_gpio.h"
#include "hw_adc.h"
#include "tps65217.h"
#include "printf.h"

SYS_APP(st);

int st_main(int argc, char **argv)
{
  uint8_t st;
  printf("BAT st: ");
  if(!pwr_charging_status(&st))
  {
    printf("0x%02x", st);
    if(st & 0x08) printf(" - charging", st);
    printf("\n");
  }
  else printf("error \n");

  uint32_t t = 0;
  int cnt = 32;
  while(cnt--)
  {
    while(!(VBAT_ADC->SR & ADC_SR_EOC));
    VBAT_ADC->SR &= ~ADC_SR_EOC;
    t += adc_val(VBAT_ADC);
  }
  t >>= 5;
  double ret = t * 0.233;
  printf("VBAT: %d \n", (int)ret);

  return 0;
}


