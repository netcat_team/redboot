/*
 * gms.c
 *
 *  Created on: Aug 26, 2019
 *      Author: netcat
 */

#include "../bsp/gma.h"
#include "sys/cli/printf.h"
#include "sys/cli/apps.h"
#include "bsp/bsp.h"
#include "bsp/tps65217.h"
#include "hw_gpio.h"
#include "hw_ltdc.h"
#include "hw_dma2d.h"
#include "hw_rng.h"

SYS_APP(gl);

int gl_main(int argc, char **argv)
{
  int j = 1024;
  while(j--)
  {
    uint32_t t = rng_data();
    uint8_t x = t >> 24;
    uint8_t y = t >> 16;
    uint8_t w = t >> 8;
    uint8_t h = t;

    if(x + w > LCD_WIDTH) w = LCD_WIDTH - x--;
    if(y + h > LCD_HEIGHT) h = LCD_HEIGHT - y--;
    if(!w) w = 1;
    if(!h) h = 1;

    gma_set_color(rng_data());
    gma_fill_rect_dma_clut(x, y, w, h * 2);
  }

  return 0;
}


