/*
 * i2c.c
 *
 *  Created on: Jun 30, 2019
 *      Author: netcat
 */

#include "printf.h"
#include "getopt.h"
#include "apps.h"
#include "syslog.h"
#include "vt.h"
#include "bsp.h"
#include "hw_i2c.h"
#include "s2303a.h"

SYS_APP(i2c);

void printf_mem(const void *ptr, int addr_offset, int addr_size,
    int item_per_line, int item_size, int count,
    int show_addr, int hide_null);

static void i2c_test_device(I2C_TypeDef *i2c, uint8_t addr, const char *name)
{
  uint8_t buf[1];
  const iovec v[] = { { &addr, sizeof(addr) } };
  uint32_t st = i2c_io(i2c, v, SIZE_OF_ARRAY(v), buf, sizeof(buf));
  printf("%-10s (0x%02x) %s\n", name, addr, st ? "ERR" : "OK");
}

static void i2c_test()
{
  i2c_test_device(I2C1, I2C_DEV_BME280, "BME280");
  i2c_test_device(I2C1, I2C_DEV_MPU9250, "MPU9250");
  i2c_test_device(I2C1, I2C_DEV_TPS65217, "TPS65217");
  i2c_test_device(I2C1, I2C_DEV_AK8963, "AK8963");
  i2c_test_device(I2C3, S2303A_ADDR, "S2303A");
}

static void i2c_dump(I2C_TypeDef *i2c, uint8_t addr, uint8_t offset, uint32_t size)
{
  uint8_t buf[size];

  printf("0x%02x \n", addr);

  const iovec v[] = {
      { &addr, sizeof(addr) },
      { &offset, sizeof(offset) },
  };
  if(!i2c_io(i2c, v, SIZE_OF_ARRAY(v), &buf, size))
  {
    uint32_t i;
    for(i = 0; i < size; i++)
    {
      printf("[0x%02x]: 0x%02x \n", (uint8_t)(i + offset), buf[i]);
    }
  }
  else
  {
    printf("error! \n");
  }
}

static void i2c_find(I2C_TypeDef *i2c)
{
  uint8_t st[128];
  for(uint8_t addr = 0; addr < SIZE_OF_ARRAY(st); addr++)
  {
    iovec v = { &addr, sizeof(addr) };
    st[addr] = i2c_io(i2c, &v, 1, 0, 0) ? 0 : addr;
  }
  printf_mem(st, 0, 1, 16, 1, SIZE_OF_ARRAY(st), 1, 1);
}

int i2c_main(int argc, char **argv)
{
  getopt_ctx_t ctx;
  getopt_init(&ctx);

  uint32_t addr = I2C_DEV_MPU9250;
  int n = 32; // amount
  int s = 0; // start
  int dev = 1;
  int read_flag = 0;
  int test_flag = 0;
  int find_flag = 0;
  int errata_flag = 0;

  while(getopt(&ctx, argc, argv, "d:e:a:n:s:rtf"))
  {
    switch(ctx.err)
    {
      case 'd':
        dev = atoi(ctx.arg);
        break;

      case 'e':
        errata_flag = 1;
        break;

      case 'a':
        addr = atoi(ctx.arg);
        break;

      case 'r':
        read_flag = 1;
        break;

      case 'n':
        n = atoi(ctx.arg);
        break;

      case 's':
        s = atoi(ctx.arg);
        break;

      case 't':
        test_flag = 1;
        break;

      case 'f':
        find_flag = 1;
        break;

      case 'i':
        i2c1_init();
        return 0;
        break;

      case ':':
        printf("Option '%c' requires an argument\n", ctx.opt);
        break;

      case '?':
        printf("Option '%s' unrecognized\n", ctx.arg);
        break;

      default:
        break;
    }
  }

  I2C_TypeDef *i2c_inst[] = { I2C1, I2C2, I2C3 };
  dev = MID(1, dev, SIZE_OF_ARRAY(i2c_inst)) - 1;
  I2C_TypeDef *i2c = i2c_inst[dev];

  if(read_flag) i2c_dump(i2c, addr, s, n);
  if(test_flag) i2c_test();
  if(find_flag) i2c_find(i2c);
  if(errata_flag)
  {
    printf("i2c busy: %d \n", i2c->SR2 & I2C_SR2_BUSY);
    i2c_errata(i2c, GPIOB, 7, 6);
    printf("errata fastfix done \n");
    i2c1_init();
    printf("init done \n");
  }

  return 0;
}

