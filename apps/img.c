/*
 * cat.c
 *
 *  Created on: Jul 2, 2019
 *      Author: netcat
 */

#include "printf.h"
#include "apps.h"

#include "bsp.h"
#include "memmap.h"
#include "vfs.h"
#include "syslog.h"

SYS_APP(img);

#define ADR_FILE_HEADER 0x00
#define ADR_INFO_HEADER 0x0E
#define BMP_FILE_TYPE 0x4d42

typedef struct __attribute__ ((packed))
{
  uint16_t type;
  uint32_t size;
  uint16_t reserved1;
  uint16_t reserved2;
  uint32_t offset_bit;
} bmp_file_headr_t;

typedef struct __attribute__ ((packed))
{
 uint32_t size;
 uint32_t width;
 uint32_t height;
 uint16_t planes;
 uint16_t bit_count;
 uint32_t compression;
 uint32_t size_img;
 uint32_t xpels_per_meter;
 uint32_t ypels_per_meter;
 uint32_t colors_used;
 uint32_t colors_important;
} bmp_info_t;

#define RGB565(clr) ((uint16_t) ((((clr) & 0xf8) >> 3) | (((clr) & 0xfc00) >> 5) | (((clr) & 0xf80000) >> 9)))

int bmp_load(void *buf, const char *file)
{
  FILE *f = fopen(file, "rb");
  if(f)
  {
    bmp_file_headr_t header;
    bmp_info_t info;

    fread(&header, 1, sizeof(bmp_file_headr_t), f);
    fread(&info, 1, sizeof(bmp_info_t), f);

    if(header.type != BMP_FILE_TYPE)
    {
      printf("'%s' is not BMP file \n", file);
      fclose(f);
      return 0;
    }

    uint32_t size = info.size_img;
    uint32_t bpp = info.bit_count >> 3;
    uint32_t palette_size = info.colors_used << 2;

    printf("BMP %ldx%ld, bpp: %ld \n", info.width, info.height, bpp);

    fseek(f, sizeof(bmp_file_headr_t) + sizeof(bmp_info_t) + palette_size, SEEK_SET);

    uint16_t *p = buf;
    while(size)
    {
      uint32_t t;
      fread(&t, 1, bpp, f);
      size -= bpp;
      *p++ = bpp == 3 ? RGB565(t) : t;
    }

    printf("BMP %ld bytes load done \n", info.size_img);
    fclose(f);
  }
  else
  {
    printf("cant open '%s'\n", file);
  }
  return 0;
}

int img_main(int argc, char **argv)
{
  if(argc < 2)
  {
    printf("file name required \n");
    return 0;
  }
  return bmp_load(LCD_FB_PTR, argv[1]);
}


