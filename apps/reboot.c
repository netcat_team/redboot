/*
 * ls.c
 *
 *  Created on: Jul 3, 2019
 *      Author: netcat
 */

#include "sys/cli/printf.h"
#include "sys/cli/apps.h"
#include "bsp/bsp.h"

SYS_APP(reboot);

#define WDT_CFG_ACCESS 0x5555
#define WDT_CFG_RELOAD 0xAAAA
#define WDT_CFG_ENABLE 0xCCCC

void wdt_reset()
{
  IWDG->KR = WDT_CFG_ACCESS;
  IWDG->PR = IWDG_PR_PR_0;
  IWDG->RLR = 1;
  IWDG->KR = WDT_CFG_RELOAD;
  IWDG->KR = WDT_CFG_ENABLE;
}

int reboot_main(int argc, char **argv)
{
  printf("The system was restarted \n");
  wdt_reset();
  while(1);
  return 0;
}
